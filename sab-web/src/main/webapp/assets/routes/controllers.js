/* global $scope, data, $http, $timeout, $location */

// Creación del módulo
var angularRoutingApp = angular.module('angularRoutingApp');

//**********************************pruebas para la ventana modal solo para inactivar elementos*********************************************************

var ClientesArticulos;
angularRoutingApp.controller('Controller', function($scope, ModalService, $http, localStorageService) {

    $scope.currentPage = 1;
    $scope.pageSize = 5;

    $scope.ImpuestoRegimen=localStorageService.get("angular-ImpuestoRegimen",$scope.ImpuestoRegimen);
    $scope.operacionImp=localStorageService.get("angular-operacionImp",$scope.operacionImp);
    $scope.grupos=localStorageService.get("angular-grupos",$scope.grupos);
    $scope.puntos=localStorageService.get("angular-puntos",$scope.puntos);
    $scope.maximominimo=localStorageService.get("angular-maximominimo",$scope.maximominimo);
    $scope.canales=localStorageService.get("angular-canales",$scope.canales);
    $scope.conf=localStorageService.get("angular-conf",$scope.conf);
    $scope.postsMarcas=localStorageService.get("angular-postsMarcas",$scope.postsMarcas);
    $scope.postsBodegas=localStorageService.get("angular-postsBodegas",$scope.postsBodegas);
    $scope.postsClientes=localStorageService.get('angular-postClientes');
    $scope.unidadCompras=localStorageService.get("angular-unidadCompra",$scope.unidadCompra);
    $scope.articulos=localStorageService.get("angular-grupos",$scope.articulos);
    $scope.ImpuestoRegimen;
    articulos = $scope.articulos;
    $scope.clientesArticulos;
    $scope.clientesArticulosAsoc;    
    $scope.myDataArticulos;
    $scope.myData;
    $scope.myDataImp;
    $scope.conf;
    $scope.clienteGrupo;
    $scope.postsCliente = localStorageService.get("angular-postsCliente",$scope.postsCliente);
   
    //funcion para borrar datos de las tablas configRegimen.
    $scope.removeDataImp = function($event){
        $scope.ImpuestoRegimen.splice($scope.ImpuestoRegimen.indexOf($event.currentTarget.id),1);
        localStorageService.set("angular-myData",$scope.ImpuestoRegimen);
        localStorageService.set("angular-ImpuestoRegimen",$scope.ImpuestoRegimen);        
    };
    
    //funcion para borrar impuesto canal de la tabla 
    $scope.removeDataImpCanal = function(nameCanal, nameImpuesto){                             
        for(var i = 0; i < $scope.operacionImp.length ; i++){
            var index;
            if(nameCanal === $scope.operacionImp[i].canal && nameImpuesto === $scope.operacionImp[i].impuesto){                
                index = $scope.operacionImp.indexOf($scope.operacionImp[i]);                                
                $scope.operacionImp.splice(index, 1);
            }
        }                        
        localStorageService.set("angular-myDataImp",$scope.operacionImp);
    };
    
    //funcion borrado dato impuesto regimen tabla
    $scope.eliminarImpReg = function(nombre) {        
        var index;  
        for (var i = 0; i < $scope.conf.length; i++) {
            if(nombre === $scope.conf[i].nombre){                
                index = $scope.conf.indexOf($scope.conf[i]);
                $scope.conf.splice(index, 1);
            }
        }       
    };
    
    //funcion borrado marcas crear punto
    $scope.eliminarMarcas = function($event) {
       $scope.postsMarcas.splice($event, 1);
       localStorageService.set("angular-postsMarcas",$scope.postsMarcas);
    };
    
    //funcion borrado bodegas crear punto
    $scope.eliminarBodega = function(nameBodega) {
        var index;        
        for (var i = 0; i < $scope.postsBodegas.length; i++) {
            if (nameBodega === $scope.postsBodegas[i].nombre) {                
                index = $scope.postsBodegas.indexOf($scope.postsBodegas[i]);
                $scope.postsBodegas.splice(index, 1);
            }
        }
        localStorageService.set("angular-postsBodegas",$scope.postsBodegas);
    };
    
    //funcion remover articulo tabla
    $scope.removeArticulo = function(namePunto) {
        var index;
        for (var i = 0; i < $scope.puntos.length; i++) {
            if(namePunto === $scope.puntos[i].nombre) {
                index = $scope.puntos.indexOf($scope.puntos[i]);
                $scope.puntos.splice(index,1);
            }
        }
        localStorageService.set("angular-puntos",$scope.puntos);
    };
    
    //funcion eliminar grupos corporativos activos    
    $scope.eliminarClienteGrupo = function(nombreCliente) {
        var index;
        for (var i = 0; i < $scope.clienteGrupo.length; i++) {
            if (nombreCliente === $scope.clienteGrupo[i].nombre) {
                index = $scope.clienteGrupo.indexOf($scope.clienteGrupo[i]);
                $scope.clienteGrupo.splice(index, 1);
            }
        }
    };
   



     $scope.show = function(id,ruta, accion) {
        console.log("Valor de la accion es  "+accion);
        console.log("Valor final ruta "+ruta+id);
        ModalService.showModal({
            templateUrl: 'modal.html',
            controller: "ModalController"
        }).then(function(modal) {
            modal.element.modal();
            modal.close.then(function(result) {
                if(result==="Yes" && accion==="inactivar"){
                    $http.delete(ruta+id);
                    location.reload();
                }if(result==="Yes" && accion==="activar"){
                    $http.put(ruta+id);
                    location.reload();
                }
                else{
                   $scope.message = ""; 
                }
                
            });
        });
    };
    
    
    
//    angularRoutingApp.controller('ModalInactivarActivarController', function ($scope, close) {
//
//        $scope.close = function (result) {
//            close(result, 500); // close, but give 500ms for bootstrap to animate
//        };
//
//    });
//    
    
   //******************************************************************************************************* 
     $scope.showEliminarArt = function(ruta, accion) {
        console.log("Valor de la accion es  "+accion);
        console.log("Valor final ruta "+ruta);
        ModalService.showModal({
            templateUrl: 'modal.html',
            controller: "ModalEliminarArtController"
        }).then(function(modal) {
            modal.element.modal();
            modal.close.then(function(result) {
                if(result==="Yes" && accion==="inactivar"){
                    $http.delete(ruta);
                    location.reload();
                }if(result==="Yes" && accion==="activar"){
                    $http.put(ruta);
                    location.reload();
                }
                else{
                   $scope.message = ""; 
                }
                
            });
        });
    };
 //***********************************************************************************************************   
 
 $scope.showArticulos = function() {
        if (localStorageService.get("angular-articulos")) {
            $scope.articulos = localStorageService.get("angular-articulos");
        } else {
            $scope.articulos = [];
        }
        ModalService.showModal({
            templateUrl: 'modalarticulos.html',
            controller: "ModalArticulosController"
        }).then(function(modal) {
            modal.element.modal();
            modal.close.then(function(result) {

               $scope.articulos= result;
               localStorageService.set("angular-grupos",$scope.articulos);
               console.log($scope.articulos);
            });
        });
    };
 
 
    $scope.showGrupoClase = function() {
        //$scope.grupos=[];
        if (localStorageService.get("angular-grupos")) {
            $scope.grupos = localStorageService.get("angular-grupos");
        } else {
            $scope.grupos = [];
        }
        ModalService.showModal({
            templateUrl: 'modalgrupoclase.html',
            controller: "ModalGrupoClaseController"
        }).then(function(modal) {
            modal.element.modal();
            modal.close.then(function(result) {

                $scope.grupos= result;
                
                localStorageService.set("angular-grupos",$scope.grupos);
                
                console.log("Valor del scope");
                console.log($scope.grupos);

            });
        });
    };
 
 
 
    $scope.showPuntos = function() {       
        if (localStorageService.get("angular-puntos")) {
            $scope.puntos = localStorageService.get("angular-puntos");
        } else {
            $scope.puntos = [];
        }
        ModalService.showModal({
            templateUrl: 'modalpuntos.html',
            controller: "ModalPuntosController"
        }).then(function(modal) {
            modal.element.modal();
            modal.close.then(function(result) {          
                $scope.puntos= result;                
                localStorageService.set("angular-puntos",$scope.puntos);                
                console.log("Valor del scope de puntos");
                console.log($scope.puntos);
            });
        });
    };
    
    $scope.showMaxMin = function() {
        //$scope.maximominimo=[];
        if (localStorageService.get("angular-maximominimo")) {
            $scope.maximominimo = localStorageService.get("angular-maximominimo");
        } else {
            $scope.maximominimo = [];
        }
        ModalService.showModal({
            templateUrl: 'modalmaxmin.html',
            controller: "ModalMaxMinController"
        }).then(function(modal) {
            modal.element.modal();
            modal.close.then(function(result) {

               
                
                
                $scope.maximominimo= result;
                
                localStorageService.set("angular-maximominimo",$scope.maximominimo);
                
                console.log("Valor del scope");
                console.log($scope.maximominimo);
                });
            });
    };
    
      $scope.showUnidadCompra = function() {
        if (localStorageService.get("angular-unidadCompra")) {
            $scope.unidadCompra = localStorageService.get("angular-unidadCompra");
        } else {
            $scope.unidadCompra = [];
        }

        ModalService.showModal({
            templateUrl: 'modalunidadcompra.html',
            controller: "ModalUnidadCompraController"
        }).then(function(modal) {
            modal.element.modal();
            modal.close.then(function(result) {

               $scope.unidadCompra= result;
               localStorageService.set("angular-unidadCompra",$scope.unidadCompra);
               console.log($scope.unidadCompra);

            });
        });
    };
    
    $scope.showBodegas = function() {
        if (localStorageService.get("angular-postsBodegas")) {
            $scope.postsBodegas = localStorageService.get("angular-postsBodegas");
        } else {
            $scope.postsBodegas = [];
        }
        
        ModalService.showModal({
            templateUrl: 'modalbodegas.html',
            controller: "ModalBodegasController"
        }).then(function(modal) {
            modal.element.modal();
            modal.close.then(function(result) {
                
              $scope.postsBodegas= result;

               console.log($scope.postsBodegas);
               localStorageService.set("angular-postsBodegas",$scope.postsBodegas);
            });
        });
    };
    
    
    $scope.showUnidadPrincipal = function() {
        
        console.log("paso por showUnidadPrincipal");
        ModalService.showModal({
            templateUrl: 'modalunidadprincipal.html',
            controller: "ModalUnidadPrincipalController"
        }).then(function(modal) {
            modal.element.modal();
            modal.close.then(function(result) {
                if(result==="Yes"){
                    $scope.message = "Mostrar tabla"; 
                }
                else{
                   $scope.message = ""; 
                }
                
            });
        });
    };
    
    
     $scope.showUnidadAlterna = function() {
        
        console.log("paso por showUnidadAlterna");
        ModalService.showModal({
            templateUrl: 'modalunidadalterna.html',
            controller: "ModalUnidadAlternaController"
        }).then(function(modal) {
            modal.element.modal();
            modal.close.then(function(result) {
                if(result==="Yes"){
                    $scope.message = "Mostrar tabla"; 
                }
                else{
                   $scope.message = ""; 
                }                
            });
        });
    };
    
    
    $scope.showGrupoSeleccion = function() {
        
        console.log("paso por showGrupoSeleccion");
        ModalService.showModal({
            templateUrl: 'modalgruposeleccion.html',
            controller: "ModalGrupoSeleccionController"
        }).then(function(modal) {
            modal.element.modal();
            modal.close.then(function(result) {
                if(result==="Yes"){
                    $scope.message = "Mostrar tabla"; 
                }
                else{
                   $scope.message = ""; 
                }
                
            });
        });
    };
    
     $scope.showCanales = function() {
        if (localStorageService.get("angular-Canales")) {
            $scope.Canales = localStorageService.get("angular-Canales");
        } else {
            $scope.Canales = [];
        }

        ModalService.showModal({
            templateUrl: 'modalcanales.html',
            controller: "ModalCanalesController"
        }).then(function(modal) {
            modal.element.modal();
            modal.close.then(function(result) {
                
                $scope.Canales= result;

                localStorageService.set("angular-Canales",$scope.Canales);
                
                console.log("Valor del scope Canales");
                console.log($scope.Canales);

            });
        });
        
    };

    $scope.showIngredientes = function() {
        
        console.log("paso por showIngredientes");
        ModalService.showModal({
            templateUrl: 'modalingredientes.html',
            controller: "ModalIngredientesController"
        }).then(function(modal) {
            modal.element.modal();
            modal.close.then(function(result) {
                if(result==="Yes"){
                    $scope.message = "Mostrar tabla"; 
                }
                else{
                   $scope.message = ""; 
                }
                
            });
        });
    };
    
    
    $scope.showClientes = function() {
        
        console.log("paso por showClientes");
        ModalService.showModal({
            templateUrl: 'modalclientes.html',
            controller: "ModalClientesController"
        }).then(function(modal) {
            modal.element.modal();
            modal.close.then(function(result) {
                if(result==="Yes"){
                    $scope.message = "Mostrar tabla"; 
                }
                else{
                   $scope.message = ""; 
                }
                
            });
        });
    };
    
    
    $scope.showPermisos = function() {
        
        console.log("paso por showPermisos");
        ModalService.showModal({
            templateUrl: 'modalpermisos.html',
            controller: "ModalPermisosController"
        }).then(function(modal) {
            modal.element.modal();
            modal.close.then(function(result) {
                if(result==="Yes"){
                    $scope.message = "Mostrar tabla"; 
                }
                else{
                   $scope.message = ""; 
                }
                
            });
        });
    };
    
    
    $scope.showRoles = function() {
        
        console.log("paso por showRoles");
        ModalService.showModal({
            templateUrl: 'modalroles.html',
            controller: "ModalRolesController"
        }).then(function(modal) {
            modal.element.modal();
            modal.close.then(function(result) {
                if(result==="Yes"){
                    $scope.message = "Mostrar tabla"; 
                }
                else{
                   $scope.message = ""; 
                }
                
            });
        });
    };
    
    $scope.showMarcas = function() {

if (localStorageService.get("angular-postsMarcas")) {
            $scope.postsMarcas = localStorageService.get("angular-postsMarcas");
        } else {
            $scope.postsMarcas = [];
        }
        ModalService.showModal({
            templateUrl: 'modalmarcas.html',
            controller: "ModalMarcasController"
        }).then(function(modal) {
            modal.element.modal();
            modal.close.then(function(result) {
              $scope.postsMarcas= result;
               console.log($scope.postsMarcas);
               //localStorageService.set("angular-postsMarcas",$scope.postsMarcas);
            });
        });
    };
  
    $scope.showConfRegimen = function() {
        //$scope.ImpuestoRegimen= [];
   
        if (localStorageService.get("angular-ImpuestoRegimen")) {
            $scope.ImpuestoRegimen = localStorageService.get("angular-ImpuestoRegimen");
        } else {
            $scope.ImpuestoRegimen = [];
        }
       
   
        ModalService.showModal({
            templateUrl: 'modalconfregimen.html',
            controller: "ModalConfRegimenController"
        }).then(function(modal) {
            modal.element.modal();
            modal.close.then(function(result) {
                
                $scope.ImpuestoRegimen= result;

                localStorageService.set("angular-ImpuestoRegimen",$scope.ImpuestoRegimen);
                
                console.log("Valor del scope ImpuestoRegimen");
                console.log($scope.ImpuestoRegimen);

            });
        });
        
    };
 
    
    
    $scope.showOperacionImp = function() {
        //$scope.operacionImp=[];
        
        if (localStorageService.get("angular-operacionImp")) {
            $scope.operacionImp = localStorageService.get("angular-operacionImp");
        } else {
            $scope.operacionImp = [];
        }
        ModalService.showModal({
            templateUrl: 'modaloperacionimp.html',
            controller: "ModalOperacionImpController"
        }).then(function(modal) {
            modal.element.modal();
            modal.close.then(function(result) {

                $scope.operacionImp= result;
                
                //localStorageService.set("angular-operacionImp",$scope.operacionImp);
                
                console.log("Valor del scope");
                console.log($scope.operacionImp);

            });
        });
    };
    
    $scope.showConfPunto = function () {

        console.log(localStorageService.get("angular-conf"));
        if (localStorageService.get("angular-conf")) {
            $scope.conf = localStorageService.get("angular-conf");
        } else {
            $scope.conf = [];
        }

        ModalService.showModal({
            templateUrl: 'modalconfpunto.html',
            controller: "ModalConfPuntoController"
        }).then(function (modal) {
            modal.element.modal();
            modal.close.then(function (result) {
                $scope.conf = result;
                console.log($scope.conf);
            });
        });
    };
    
    $scope.showProveedor = function() {
        ModalService.showModal({
            templateUrl: 'modalproveedor.html',
            controller: "ModalProveedorController"
        }).then(function(modal) {
            modal.element.modal();
            modal.close.then(function(result) {
                $scope.conf= result;
               console.log($scope.conf);
                
            });
        });
    };
 
});



angularRoutingApp.controller('ModalController', function($scope, close, $http) {
 
    $scope.posts = [];
    $http.get('./rest/articulo/?mostrar=activos/')
            .success(function (data) {

                console.log(data);
                $scope.posts = data;
            })
            .error(function (err) {

                alert("Ha fallado la petición HTTP " + err);
            });
 $scope.close = function(result) {
 	close(result, 500); // close, but give 500ms for bootstrap to animate
 };

});


angularRoutingApp.controller('ModalArticulosController', function($scope, close, $http, localStorageService) {
 
   
    $scope.formData = [];
    if (localStorageService.get("angular-myDataArticulos")) {
        $scope.myDataArticulos = localStorageService.get("angular-myDataArticulos");
    } else {
        $scope.myDataArticulos = [];
    }
    $http.get('./rest/articulo/?mostrar=activos/')
            .success(function (data) {

                console.log(data);
                $scope.posts = data;
            })
            .error(function (err) {

                console.log("Ha fallado la petición HTTP " + err);
            });
            
    $scope.close = function(result) {

           console.log($scope.formData);
           result= $scope.myDataArticulos;
           close(result, 500); // close, but give 500ms for bootstrap to animate                		    
    };
 
    $scope.addData = function() {

         console.log($scope.formData);
         $scope.myDataArticulos.push(
            {
               'idArticulo': $scope.formData.asociarArticulo.id,
               'idGrupoSeleccion': null,
               'idUnidad': 0,
               'incrementoPrecio': $scope.formData.incrementa,
               'nombreArticulo': $scope.formData.asociarArticulo.nombre,
               'cantidadADescargar': $scope.formData.kardex,
               'nombreUnidad': $scope.formData.unidad.nombre

            });

            $scope.formData="";
            localStorageService.set("angular-myDataArticulos",$scope.myDataArticulos);

    };

    $scope.removeData = function(selData) {
            $scope.myDataClase.splice($scope.myDataClase.indexOf(selData),1);
    };

});


angularRoutingApp.controller('ModalGrupoClaseController', function($scope, close, localStorageService) {
 if (localStorageService.get("angular-myDataClase")) {
                $scope.myDataClase = localStorageService.get("angular-myDataClase");
                } else {
                $scope.myDataClase = [];
                }
                
                 $scope.close = function(result) {
                     result=$scope.myDataClase;
                     console.log("Valor de $scope.myDataClase");
                    console.log($scope.myDataClase);
                    
                        close(result, 500); // close, but give 500ms for bootstrap to animate

                 };
                 
                 
                $scope.addData = function() {
                    
                    $scope.myDataClase.push(
                        {
                           'id': 0,
                           'nombre': $scope.inData
                           
                        });
                    
                    
                    
			
			$scope.inData="";
                        localStorageService.set("angular-myDataClase",$scope.myDataClase);
		};
		$scope.removeData = function(selData) {
			$scope.myDataClase.splice($scope.myDataClase.indexOf(selData),1);
		};

});


angularRoutingApp.controller('ModalPuntosController', function($scope, close, $http) {
 
   
    $scope.formData = [];
    $scope.puntos = [];
    $http.get('./rest/punto/')
            .success(function (data) {

                console.log(data);
                $scope.posts = data;
            })
            .error(function (err) {

                console.log("Ha fallado la petición HTTP " + err);
            });
 $scope.close = function(result) {
     console.log($scope.formData);
        result= $scope.formData.asociarPunto;
        
 	close(result, 500); // close, but give 500ms for bootstrap to animate
         
 };

});

angularRoutingApp.controller('ModalMaxMinController', function($scope, close, $http, localStorageService) {
    $scope.formData = [];
    //$scope.myData = [];
    $scope.posts = [];
    $http.get('./rest/punto/')
            .success(function (data) {

                console.log(data);
                $scope.posts = data;
            })
            .error(function (err) {

                console.log("Ha fallado la petición HTTP " + err);
            });
            
            if (localStorageService.get("angular-myDataMaxMin")) {
                $scope.myDataMaxMin = localStorageService.get("angular-myDataMaxMin");
                } else {
                $scope.myDataMaxMin = [];
                }
            
 $scope.close = function(result) {
                     result=$scope.myDataMaxMin;
                    console.log($scope.myDataMaxMin);
                    
                        close(result, 500); // close, but give 500ms for bootstrap to animate

                 };
                $scope.addData = function() {
                    console.log($scope.formData);
			$scope.myDataMaxMin.push($scope.formData);
			$scope.formData="";
                        localStorageService.set("angular-myDataMaxMin",$scope.myDataMaxMin);
		};
		$scope.removeData = function(selData) {
			$scope.myDataMaxMin.splice($scope.myDataMaxMin.indexOf(selData),1);
		};

});

angularRoutingApp.controller('ModalUnidadCompraController', function($scope, close, localStorageService) {
$scope.formData=[];
     if (localStorageService.get("angular-myDataUnidad")) {
                $scope.myDataUnidad = localStorageService.get("angular-myDataUnidad");
                } else {
                $scope.myDataUnidad = [];
                }

                 $scope.close = function(result) {
                     result=$scope.myDataUnidad;
                    console.log($scope.myDataUnidad);
                        close(result, 500); // close, but give 500ms for bootstrap to animate

                 };
                $scope.addData = function() {
                    console.log("paso por add unidad alterna de compra");
                    console.log($scope.formData);
			$scope.myDataUnidad.push($scope.formData);
console.log($scope.myDataUnidad);
                        localStorageService.set("angular-myDataUnidad",$scope.myDataUnidad);
                        
                        
                        
		};
		$scope.removeData = function(selData) {
			$scope.myDataUnidad.splice($scope.myDataUnidad.indexOf(selData),1);
		};

});

angularRoutingApp.controller('ModalBodegasController', function($scope, close, $http) {
 $scope.formData = [];
    $scope.posts = [];
    $http.get('./rest/bodegas/')
            .success(function (data) {

                console.log(data);
                $scope.posts = data;
            })
            .error(function (err) {

                console.log("Ha fallado la petición HTTP " + err);
            });
 $scope.close = function(result) {
     console.log($scope.formData.asociarBodega);
     result= $scope.formData.asociarBodega;
 	close(result, 500); // close, but give 500ms for bootstrap to animate
 };

});

angularRoutingApp.controller('ModalUnidadPrincipalController', function($scope, close, $http) {
 
    $scope.posts = [];
    var id=1;
    $http.get("./rest/cliente/"+id+"/listasPrecios")
            .success(function (data) {

                console.log(data);
                $scope.posts = data;
            })
            .error(function (err) {

                alert("Ha fallado la petición HTTP " + err);
            });
 $scope.close = function(result) {
 	close(result, 500); // close, but give 500ms for bootstrap to animate
 };

});


angularRoutingApp.controller('ModalUnidadAlternaController', function($scope, close, $http) {
 
    $scope.posts = [];
    var id=1;
    $http.get("./rest/cliente/"+id+"/listasPrecios")
            .success(function (data) {

                console.log(data);
                $scope.posts = data;
            })
            .error(function (err) {

                alert("Ha fallado la petición HTTP " + err);
            });
 $scope.close = function(result) {
 	close(result, 500); // close, but give 500ms for bootstrap to animate
 };

});


angularRoutingApp.controller('ModalGrupoSeleccionController', function($scope, close, $http) {
 
    $scope.posts = [];
    $http.get("./rest/grupoSeleccion/?mostrar=activos")
            .success(function (data) {

                console.log(data);
                $scope.posts = data;
            })
            .error(function (err) {

                alert("Ha fallado la petición HTTP " + err);
            });
 $scope.close = function(result) {
 	close(result, 500); // close, but give 500ms for bootstrap to animate
 };

});


angularRoutingApp.controller('ModalCanalesController', function($scope, close, $http, localStorageService) {
 //$scope.formData=[];
    $scope.posts = [];
    $http.get("./rest/common/canales/")
            .success(function (data) {

                console.log(data);
                $scope.posts = data;
            })
            .error(function (err) {

                alert("Ha fallado la petición HTTP " + err);
            });
                if (localStorageService.get("angular-myDataCanal")) {
                $scope.myDataCanal = localStorageService.get("angular-myDataCanal");
                } else {
                $scope.myDataCanal = [];
                }

                
                 $scope.close = function(result) {
                     result=$scope.myDatamyDataCanal;
                    console.log($scope.myDatamyDataCanal);
                    
                        close(result, 500); // close, but give 500ms for bootstrap to animate
                            
                 };
                $scope.addData = function() {
                    

			$scope.myDatamyDataCanal.push($scope.formData.asociarCanal);
                        
                        

			$scope.formData=[];
 localStorageService.set("angular-myDatamyDataCanal",$scope.myDatamyDataCanal);

                      
		};
		$scope.removeData = function(selData) {
			$scope.myDatamyDataCanal.splice($scope.myDatamyDataCanal.indexOf(selData),1);
		};

});

angularRoutingApp.controller('ModalIngredientesController', function($scope, close, $http) {
 
    $scope.posts = [];
    $http.get("./rest/articulo/?mostrar=activos/")
            .success(function (data) {

                console.log(data);
                $scope.posts = data;
            })
            .error(function (err) {

                console.log("Ha fallado la petición HTTP " + err);
            });
 $scope.close = function(result) {
 	close(result, 500); // close, but give 500ms for bootstrap to animate
 };

});


angularRoutingApp.controller('ModalClientesController', function($scope, close, $http, localStorageService) {
    if (localStorageService.get("angular-dataCliente")) {
        $scope.postsCliente = localStorageService.get("angular-dataCliente");
    } else {
        $scope.postsCliente = [];
    }
 
    $scope.posts = [];    
    $http.get("./rest/cliente/")
            .success(function (data) {
                $scope.postsClientes = data;
                console.log($scope.postsClientes);                
                $scope.posts = data;
            })
            .error(function (err) {

                alert("Ha fallado la petición HTTP " + err);
            });
            
    var item;
    $scope.selection = function(position, posts) {      
      angular.forEach(posts, function (sub, index) {
          if(position !== index) {
              sub.checked = false;              
              //console.log($scope.postsClientes);
          };
          if(sub.checked === true){
            
            item = [{
                 id:sub.id, 
                 name: sub.nombre
            }];            
          }
      });         
    };
    
    $scope.addData = function() {      
      localStorageService.set("angular-dataCliente", item);
      $scope.postsCliente[0] = item;
      console.log($scope.postsCliente);
    };
    
    $scope.addDataCliente = function() {
      localStorageService.set("angular-clienteUser", item);
      
      $scope.clientUSer = localStorageService.get("angular-dataClienteUser");
      //localStorage.setItem('angular-dataCliente', JSON.stringify(item));      
      localStorageService.set("angular-dataCliente", item);
      console.log('sacando dato dataCliente del localSatorage');
      console.log(localStorageService.get("angular-dataCliente"));
      console.log(item[0].id);
      $http.get('./rest/cliente/' + item[0].id)
              .success(function (data) {                  
                  localStorageService.set('infoCliente', data);
                  console.log(':(');
                  //localStorage.setItem('infoCliente', JSON.stringify(data));
              })
              .error(function(err) {
                  alert('error en peticion http: ' + err);
              });
       
    };
    $scope.clientesArticulos = ['35', 'ffsdf', 'wrr3r', 'xcvcxvxvz', 's3sfsf'];
    console.log(localStorageService.get("angular-clienteArticulo"));
    $scope.close = function(result) {
       console.log(result);
       close(result, 500); // close, but give 500ms for bootstrap to animate 
       if(result === 'Yes'){
           location.reload();
       };
       
    };

});


angularRoutingApp.controller('ModalPermisosController', function($scope, close, $http) {
 
    $scope.posts = [];
    var id = 1;
    $http.get("./rest/rolcliente/transacciones/")
            .success(function (data) {

                console.log(data);
                $scope.posts = data;
            })
            .error(function (err) {

                alert("Ha fallado la petición HTTP " + err);
            });
 $scope.close = function(result) {
 	close(result, 500); // close, but give 500ms for bootstrap to animate
 };

});


angularRoutingApp.controller('ModalRolesController', function($scope, close, $http) {
 
    $scope.posts = [];
    var id = 1;
    $http.get("./rest/rol/")
            .success(function (data) {

                console.log(data);
                $scope.posts = data;
            })
            .error(function (err) {

                alert("Ha fallado la petición HTTP " + err);
            });
 $scope.close = function(result) {
 	close(result, 500); // close, but give 500ms for bootstrap to animate
 };

});

angularRoutingApp.controller('ModalMarcasController', function($scope, close, $http) {
 
    $scope.posts = [];
    $scope.formData = [];
    $http.get("./rest/marca/")
            .success(function (data) {
                console.log('cargando marcas modal!');
                console.log(data);
                $scope.posts = data;
            })
            .error(function (err) {
                alert("Ha fallado la petición HTTP " + err);
            });          
            
            $scope.close = function(result) {
               console.log($scope.formData.asociarMarca);
               result= $scope.formData.asociarMarca;
               close(result, 500); // close, but give 500ms for bootstrap to animate         
            };
    
            


});
//********************************* modal crear impuesto :)**************************************************
angularRoutingApp.controller('ModalConfRegimenController', function ($scope, $timeout,  close, localStorageService) {
    $scope.formData = [];

    if (localStorageService.get("angular-myData")) {
        $scope.myData = localStorageService.get("angular-myData");

    } else {
        $scope.myData = [];
    }

    if (localStorageService.get("angular-myDataId")) {
        $scope.myDataId = localStorageService.get("angular-myDataId");
    } else {
        $scope.myDataId = [];
    }

    $scope.close = function (result) {
        result = $scope.myData;
        console.log($scope.myData);

        close(result, 500); // close, but give 500ms for bootstrap to animate

    };


    $scope.addData = function () {

        if ($scope.formData.impuesto.id === 1 && $scope.banderaIva === undefined) {
            $scope.banderaIva = "crea iva";

            $scope.myData.push(
                    {
                        'idPais': $scope.formData.pais.id,
                        'pais': $scope.formData.pais.nombre,
                        'idImpuesto': $scope.formData.impuesto.id,
                        'impuesto': $scope.formData.impuesto.nombre,
                        'id': $scope.formData.regimen.id,
                        'regimen': $scope.formData.regimen.nombre

                    });

            $scope.myDataId.push(
                    {
                        'idPais': $scope.formData.pais.id,

                        'idImpuesto': $scope.formData.impuesto.id,

                        'id': $scope.formData.regimen.id


                    });
            $scope.formData = [];
            localStorageService.set("angular-myData", $scope.myData);
            localStorageService.set("angular-myDataId", $scope.myDataId);
        } else if ($scope.formData.impuesto.id === 2 && $scope.banderaImpuesto === undefined) {
            $scope.banderaImpuesto = "crea Impuesto";

            $scope.myData.push(
                    {
                        'idPais': $scope.formData.pais.id,
                        'pais': $scope.formData.pais.nombre,
                        'idImpuesto': $scope.formData.impuesto.id,
                        'impuesto': $scope.formData.impuesto.nombre,
                        'id': $scope.formData.regimen.id,
                        'regimen': $scope.formData.regimen.nombre

                    });

            $scope.myDataId.push(
                    {
                        'idPais': $scope.formData.pais.id,

                        'idImpuesto': $scope.formData.impuesto.id,

                        'id': $scope.formData.regimen.id


                    });
            $scope.formData = [];
            localStorageService.set("angular-myData", $scope.myData);
            localStorageService.set("angular-myDataId", $scope.myDataId);


        } else if ($scope.formData.impuesto.id === 3) {


            $scope.myData.push(
                    {
                        'idPais': $scope.formData.pais.id,
                        'pais': $scope.formData.pais.nombre,
                        'idImpuesto': $scope.formData.impuesto.id,
                        'impuesto': $scope.formData.impuesto.nombre,
                        'id': $scope.formData.regimen.id,
                        'regimen': $scope.formData.regimen.nombre

                    });

            $scope.myDataId.push(
                    {
                        'idPais': $scope.formData.pais.id,

                        'idImpuesto': $scope.formData.impuesto.id,

                        'id': $scope.formData.regimen.id


                    });
            $scope.formData = [];
            localStorageService.set("angular-myData", $scope.myData);
            localStorageService.set("angular-myDataId", $scope.myDataId);


        } else {

            $scope.showMessage = true;
            
            $timeout(function() {
                $scope.showMessage = false;               
            }, 3000);
            
            $scope.message = "El Impuesto seleccionado ya fue configurado";
            $scope.ruta = "modalconfregimen.html";
            $scope.tipo = "alert-info";            
        }



    };
    console.log('creacion del impuesto');
    console.log($scope.myData);

    $scope.removeData = function (selData) {
        
        $scope.myData.splice($scope.myData.indexOf(selData), 1);
        localStorageService.set("angular-myData", $scope.myData);
        //$scope.ImpuestoRegimen = $scope.myData;
        result = $scope.myData;
    };

});
//******************************** fin controlador modal crear impuestos ****************************************

angularRoutingApp.controller('ModalOperacionImpController', function ($scope, close, localStorageService) {

    //$scope.myData = [];
    $scope.btn;
    $scope.formData = [];
    if (localStorageService.get("angular-myDataImp")) {
        $scope.myDataImp = localStorageService.get("angular-myDataImp");
    } else {
        $scope.myDataImp = [];
    }

    $scope.close = function (result) {
        result = $scope.myDataImp;
        console.log($scope.myDataImp);

        close(result, 500); // close, but give 500ms for bootstrap to animate

    };
    $scope.addData = function () {
        console.log("elimina la opc");
        var x = document.getElementById("canal");
        x.remove(x.selectedIndex);
        console.log($scope.formData);
        if ($scope.formData.impmasusado === true) {
            $scope.formData.impmasusado = "SI";
        } else {
            $scope.formData.impmasusado = "NO";
        }
        if ($scope.formData.unicatarifa === true) {
            $scope.formData.unicatarifa = "SI";
        } else {
            $scope.formData.unicatarifa = "NO";
        }
        $scope.myDataImp.push(
                {
                    'canal': $scope.formData.canal.nombre,
                    'impuesto': $scope.formData.impuesto.impuesto,
                    'regimen': $scope.formData.impuesto.regimen,
                    'impmasusado': $scope.formData.impmasusado,
                    'unicatarifa': $scope.formData.unicatarifa
                });
        $scope.formData = [];
        localStorageService.set("angular-myDataImp", $scope.myDataImp);
        if($scope.myDataImp.length !== 0){            
            $scope.btn = true;
        };

    };
    $scope.removeDataImpCanal = function (selData) {
        $scope.myDataImp.splice($scope.myDataImp.indexOf(selData), 1);
        localStorageService.set("angular-myDataImp", $scope.myDataImp);
        result = $scope.myDataImp;
        if($scope.myDataImp.length === 0){            
            $scope.btn = false;
        };
    };
    


});

angularRoutingApp.controller('ModalEliminarArtController', function($scope, close, $http) {
 
    $scope.posts = [];
    $http.get('./rest/punto/')
            .success(function (data) {

                console.log(data);
                $scope.posts = data;
            })
            .error(function (err) {

                alert("Ha fallado la petición HTTP " + err);
            });
 $scope.close = function(result) {
 	close(result, 500); // close, but give 500ms for bootstrap to animate
        $scope.formData={};
        console.log($scope.formData);
 };

});

angularRoutingApp.controller('ModalConfPuntoController', function($scope, close ) {
 
$scope.formData = [];
 
     var data = [
         {nombre:'Maneja bodega'},
         {nombre:'Permitir almacenar en el punto'},
         {nombre:'Permitir apoyo de licencia en red'},
         {nombre:'Autoriza anular verificacion'},
         {nombre:'Autoriza usuario que elimina'},
         {nombre:'Capturar codigo autoservicio'},
         {nombre:'Capturar codigo autoservicio'},
         {nombre:'Capturar codigo de mesero'},
         {nombre:'Capturar numero de personas atendidas'},
         {nombre:'Realizar Cierre oculto caja'},
         {nombre:'Solicitar clave para anular'},
         {nombre:'Requerir clave para anular verificacion'},
         {nombre:'Contar dinero por denominacion'},
         {nombre:'Identificar el control de consumo con la palabra'},
         {nombre:'Denominar persona que atiende mesa'},
         {nombre:'Facturar en formato resumido'},
         {nombre:'Permitir facturar en el punto'},
         {nombre:'Facturar sin documento de verificacion'},
         {nombre:'Permitir varias formas de venta meseros'},
         {nombre:'Permitir impresion remota'},
         {nombre:'Generar impresion remota de copia de la factura'},
         {nombre:'Imprimir tiquete de pedido autoservicio'},
         {nombre:'incluir iva en precio de venta'},
         {nombre:'Manejar varias licencias en el punto'},
         {nombre:'opLicNred'}
     ];  
     $scope.posts=data;
$scope.close = function(result) {
    console.log($scope.formData.confPunto);
        result= $scope.formData.confPunto;
 	close(result, 500); // close, but give 500ms for bootstrap to animate
         
 };
 
     $scope.selectall=function(){
     console.log("estamos todos");   
 
         
         
     };
    
//    $scope.seleccionartodos=function (){
//     $.each($scope.posts, function(post){post.checked();
//     $('#confPunto').cheked();
//       }
//     })
//
//     };
   });



angularRoutingApp.controller('ModalProveedorController', function($scope, close) {
 
   
 $scope.close = function(result) {
 	close(result, 500); // close, but give 500ms for bootstrap to animate
        $scope.formData={};
        console.log($scope.formData);
 };

});


//**********************************fin pruebas********************************************************************************************

// Controlador para las rutas
angularRoutingApp.controller('mainController', function ($scope) {

});
//****************************Controladores para procesar información de Clientes**********************************************************    
angularRoutingApp.controller('clientesController', function ($scope, $http) {
    $scope.posts = [];

    $http.get("./rest/cliente/")
            .success(function (data) {

                console.log(data);
                $scope.posts = data;
            })
            .error(function (err) {

                alert("Ha fallado la petición HTTP " + err);
            });

});

angularRoutingApp.controller('buscarclienteController', function ($scope, $http, $stateParams) {

     $scope.posts = {};
     $scope.postsPais = {};
     $scope.postsDep = {};
     $scope.postsMun = {};
     
   console.log("./rest/cliente/"+$stateParams.id);
    $http.get("./rest/cliente/"+$stateParams.id)
    
            .success(function (data) {
 
                console.log(data);               
                $scope.posts.id = data.id;
                $scope.posts.nombre = data.nombre;
                $scope.posts.nit = data.nit;
                $scope.posts.digitoChequeo = data.digitoChequeo;
                $scope.posts.direccion = data.direccion;
                $scope.posts.telefono = data.telefono;
                $scope.posts.extension = data.extension;
                $scope.posts.celular = data.celular;
                $scope.posts.email = data.email;
                $scope.posts.regimen_id = data.regimen_id;
                $scope.posts.nombreRepresentante = data.nombreRepresentante;
                $scope.posts.documentoRepresentante = data.documentoRepresentante;
                $scope.posts.nombreDueno = data.nombreDueno;
                $scope.posts.mailDueno = data.mailDueno;
                $scope.posts.celularDueno = data.celularDueno;
                $scope.posts.pais = data.municipio;
                $scope.posts.departamento = data.municipio;
                $scope.posts.municipio = data.municipio;
                
                $http.get("./rest/common/municipios/"+$scope.posts.municipio)
            .success(function (dataMunicipio) {
 console.log("dataMunicipio");
                console.log(dataMunicipio);
                $scope.postsMun.id = dataMunicipio.id;
                $scope.postsMun.nombre = dataMunicipio.nombre;
                $scope.postsMun.departamento_id = dataMunicipio.departamento_id;


                $http.get("./rest/common/dptos/"+$scope.postsMun.departamento_id)
            .success(function (dataDepartamento) {
  console.log("dataDepartamento");
                console.log(dataDepartamento);
                $scope.postsDep.id = dataDepartamento.id;
                $scope.postsDep.nombre = dataDepartamento.nombre;
                $scope.postsDep.pais_id = dataDepartamento.pais_id;


                $http.get("./rest/common/paises/"+$scope.postsDep.pais_id)
            .success(function (dataPaises) {
 console.log("dataPaises");
                console.log(dataPaises);
                $scope.postsPais.id = dataPaises.id;
                $scope.postsPais.nombre = dataPaises.nombre;
                

            })
            .error(function (err) {
                console.log("error de pais");
            });

            })
            .error(function (err) {
                console.log("Error de departamento");
            });
            })
            .error(function (err) {
                console.log("Error de municipio");
            });

            }).error(function (err) {
                console.log("Error de datos cliente");
            });

});


angularRoutingApp.controller('buscarAjustesclienteController', function ($scope, $http, $stateParams) {
    //$scope.post = {};
    //$scope.postsRegimen = {};
 
    $scope.post=$stateParams.id;
    console.log("./rest/cliente/"+$stateParams.id+"/regimenes/");
    $http.get("./rest/cliente/"+$stateParams.id+"/regimenes/")
    
            .success(function (data) {
 
                console.log(data);
                $scope.postsRegimen = data;
                
                $http.get("./rest/cliente/"+$stateParams.id+"/configuracion/")
    
            .success(function (dataConf) {

                console.log(dataConf);
                
                
                if(dataConf.empaqueDiferencialXCanal===true){
                   document.getElementById("empaqueporcanal").checked = true;
                   
                }
                if(dataConf.empaqueDiferencialXPunto===true){
                   document.getElementById("empaqueporpunto").checked = true;
                   
                }
                if(dataConf.recetaDiferencialXCanal===true){
                   document.getElementById("recetaporcanal").checked = true;
                   
                }
                if(dataConf.recetaDiferencialXPunto===true){
                   document.getElementById("recetaporpunto").checked = true;
                   
                }
                if(dataConf.unicoImpuestoXCanales===true){
                   document.getElementById("unidadventa").checked = true;
                   
                }
                
                
                
            });
                
                
        });

});


angularRoutingApp.controller('buscarclienteCanImpController', function ($scope, $http, $stateParams) {
//$scope.post = {};
$scope.post=$stateParams.id;
$http.get("./rest/cliente/"+$stateParams.id+"/canalImpuesto/")
    
                .success(function (dataCanalImp) {
                    console.log(dataCanalImp);
                    $scope.postsCanalImp = dataCanalImp;
                    
                    
                    $http.get("./rest/cliente/"+$stateParams.id+"/canales/")
    
                .success(function (dataCanal) {
                    console.log(dataCanal);
                    $scope.postsCanal = dataCanal;
                    
                    
                    
                    
                });
                    
                });

});


angularRoutingApp.controller('listaclientesinactivosController', function ($scope, $http) {
    $scope.posts = [];
    $scope.currentPage = 1;
  $scope.pageSize = 1;
    $http.get("./rest/cliente/inactivos/")
            .success(function (data) {

                console.log(data);
                $scope.posts = data;
            })
            .error(function (err) {

                alert("Ha fallado la petición HTTP " + err);
            });
});



//****************************Fin Controladores para procesar información de Clientes*******************************************************

//****************************Controladores para procesar información de Grupos Corporativos************************************************  
angularRoutingApp.controller('gruposcorporativosController', function ($scope, $http) {
    $scope.posts = [];
    $scope.currentPage = 1;
    $scope.pageSize = 1;
    $http.get("./rest/grupo/")
            .success(function (data) {

                console.log(data);
                $scope.posts = data;
            })
            .error(function (err) {

                alert("Ha fallado la petición HTTP " + err);
            });
});

angularRoutingApp.controller('listagruposcorporativosinactivosController', function ($scope, $http) {
    $scope.posts = [];
    $http.get("./rest/grupo/inactivos/")
            .success(function (data) {

                console.log(data);
                $scope.posts = data;
            })
            .error(function (err) {

                alert("Ha fallado la petición HTTP " + err);
            });
});

angularRoutingApp.controller('creargrupocorporativoController', function ($scope, $http) {
    $scope.posts = [];
    $scope.formData = {};
    
    

    $http.get("./rest/cliente/")
            .success(function (data) {

                console.log(data);
                $scope.posts = data;
            })
            .error(function (error) {

                console.log(error);
            });
            
            $scope.processFormGrupo = function() {
if($scope.formData.id===undefined){
    $scope.formData.id=1;
}

               console.log($scope.formData.id);
               console.log("Valor de longitud array"+$scope.formData.id.length);
               if($scope.formData.id!==undefined && $scope.formData.id.length>=2){
                   
                   $http.post('./rest/grupo/',
                {
                    
                   'nombre': $scope.formData.nombreGrupo

                })
                .success(function (data) {
                  
                  
                console.log(data);
                console.log("./rest/grupo/"+data+"/agregarclientes/");
                $http.post('./rest/grupo/'+data+'/agregarclientes/',$scope.formData.id);               
                $scope.message="El Grupo Corporativo se ha creado satisfactoriamente";
                   $scope.ruta="#/gruposcorporativos";
                   $scope.tipo="alert-success";
                    
                });
                   
                   
                   
               }else{
                   $scope.message="Se debe seleccionar al menos dos clientes para Crear el Grupo Corporativo";
                   $scope.ruta="creargrupocorporativo";
                   $scope.tipo="alert-warning";
               }
               console.log($scope.formData.nombreGrupo);
               console.log($scope.formData);

    
};
});

angularRoutingApp.controller('buscargrupoController', function ($scope, $http, $stateParams) {
    $scope.posts = {};
    console.log("./rest/grupo/" + $stateParams.id);
    $http.get("./rest/grupo/" + $stateParams.id)
            .success(function (data) {               
                console.log(data);
                //$scope.posts = data;
                $scope.posts.nombre = data.nombre;
                $scope.clienteGrupo = data.clientes;
                $scope.posts.clientes = $scope.clienteGrupo;                                                                
            })
            .error(function (err) {

                alert("Ha fallado la petición HTTP " + err);
                console.log(err);
            });



});


//****************************Fin Controladores para procesar información de Grupos Corporativos*******************************************************


//****************************Controladores para procesar información de Marcas************************************************************************  
        angularRoutingApp.controller('marcasController', function ($scope, $http) {
        $scope.posts = [];
                //ajuste por TT
                var currentLocation = window.location;
                if (currentLocation === "http://localhost:8080/sab/main#/marcas/created"){
        $scope.state = 'something';
        }
        console.log('valor de state ' + $scope.state);
                $http.get("./rest/marca/")
                .success(function (data) {

                console.log(data);
                        $scope.posts = data;
                })
                .error(function (err) {

                alert("Ha fallado la petición HTTP " + err);
                });
        });
        
angularRoutingApp.controller('crearmarcaController', function ($scope, $http) {
   $scope.posts = [];
   $scope.formData = {};
   $http.get("./rest/cliente/")
           .success(function (data) {

               console.log(data);
               $scope.posts = data;
           })
           .error(function (err) {

               alert("Ha fallado la petición HTTP " + err);
           });
   $scope.processFormMarca = function () {
       console.log($scope.formData.nombre);
       console.log($scope.formData.idCliente);
       
       if($scope.formData.idCliente!==undefined){
       
       $http.post('./rest/marca/',
               {
                  'id':null,
                  'idCliente':$scope.formData.idCliente,
                  'nombre':$scope.formData.nombre
               });
               $scope.message="La Marca se ha creado satisfactoriamente";
                  //$scope.ruta="#/marcas";
                  window.location.href = "#/marcas/created";
                  $scope.tipo="alert-success";
           }else{
               $scope.message="Se debe seleccionar  un cliente para crear la marca";
                  $scope.ruta="crearmarca";
                  $scope.tipo="alert-warning";
           }
   };
});

//angularRoutingApp.controller('crearmarcaController', function ($scope, $http) {
//    $scope.posts = [];
//    $scope.formData = {};
//    $http.get("./rest/cliente/")
//    .success(function (data) {
//
//    console.log(data);
//            $scope.posts = data;
//    })
//    .error(function (err) {
//
//    alert("Ha fallado la petición HTTP " + err);
//    });
//    $scope.processFormMarca = function () {
//        console.log($scope.formData.nombre);
//        console.log($scope.formData.idCliente);
//        if ($scope.formData.idCliente !== undefined){
//
//        $http.post('./rest/marca/',{
//                'id':null,
//                'idCliente':$scope.formData.idCliente,
//                'nombre':$scope.formData.nombre
//        });
//                        //ajuste por TT
//                        //$scope.message="La Marca se ha creado satisfactoriamente"; 
//                        //$scope.ruta="marcas";
//
//                window.location.href = "#/marcas/created";
//                $scope.tipo = "alert-success";
//                }).success(function (data){
//                $scope.message = "La Marca se ha creado satisfactoriamente";
//                        $scope.ruta = "#/marcas";
//                        $scope.tipo = "alert-success";
//                }).error(function (err) {
//
//                $scope.message = "La Marca ya existe";
//                        $scope.ruta = "crearmarca";
//                        $scope.tipo = "alert-warning";
//                });
//                } else{
//        $scope.message = "Se debe seleccionar un cliente para crear la marca";
//                $scope.ruta = "crearmarca";
//                $scope.tipo = "alert-warning";
//        }
//    };
//});

angularRoutingApp.controller('listamarcasinactivasController', function ($scope, $http) {
    $scope.posts = [];
    $http.get("./rest/marca/inactivos/")
            .success(function (data) {

                console.log(data);
                $scope.posts = data;
            })
            .error(function (err) {

                alert("Ha fallado la petición HTTP " + err);
            });
});

angularRoutingApp.controller('buscarmarcaController', function ($scope, $http, $stateParams, localStorageService) {    
    $scope.posts = {};    
    //console.log("./rest/marca/"+$stateParams.id);
    

    $http.get("./rest/marca/"+$stateParams.id)
            .success(function (data) {
                //console.log(data);
                $scope.posts.nombre = data.nombre;
                console.log($scope.posts);
                //console.log("./rest/cliente/"+data.idCliente);
                $http.get("./rest/cliente/"+data.idCliente)
                .success(function (dataCliente) {

                    //console.log(dataCliente);
                    if(localStorageService.get('angular-dataCliente') !== null) {
                        $scope.postsCliente = localStorageService.get('angular-dataCliente');
                    }else{
                        $scope.postsCliente = [];
                    }
                });
            });
            
    $scope.eliminarMarca = function(item) {    
      console.log(item);
      $scope.postsCliente.splice(0, 1);
    };
});



//****************************Fin Controladores para procesar información de Marcas********************************************************************


//****************************Controladores para procesar información de Usuarios*********************************************************************** 
angularRoutingApp.controller('usuariosController', function ($scope, $http) {
    $scope.posts = [];
    $http.get("./rest/user/")
            .success(function (data) {

                console.log(data);
                $scope.posts = data;
            })
            .error(function (err) {

                alert("Ha fallado la petición HTTP " + err);
            });
});

angularRoutingApp.controller('listausuariosinactivosController', function ($scope, $http) {
    $scope.posts = [];
    $http.get("./rest/user/inactivos/")
            .success(function (data) {

                console.log(data);
                $scope.posts = data;
            })
            .error(function (err) {

                alert("Ha fallado la petición HTTP " + err);
            });
});

angularRoutingApp.controller('buscarusuarioController', function ($scope, $http, $stateParams, localStorageService) {
    $scope.posts = [];
    $scope.postsCliente = [];
    $scope.formData = {};
    $scope.id = $stateParams.id;
    
    $scope.usuariosDatos = localStorage.getItem('infoCliente');
    console.log(localStorage.getItem('infoCliente'));
    console.log('cargando info por partes!');
    console.log(typeof($scope.usuariosDatos));
    $http.get("./rest/user/" + $stateParams.id)
            .success(function (data) {

                console.log(data);                
                $scope.posts.fullName = data.fullName;
                $scope.posts.username = data.username;
                $scope.posts.password = data.password;
                $scope.posts.password = data.password;
            })
            .error(function (err) {
                alert("Ha fallado la petición HTTP " + err);
            });
    console.log($stateParams.id);
    $scope.postsCliente = [];
    $http.get("./rest/cliente/" + $stateParams.id)
            .success(function (data) {

                console.log(data);                
                console.log($stateParams.id);
                console.log('************************** info cliente **********************');
                $scope.postsCliente.nombre = data.nombre;
                $scope.postsCliente.nit = data.nit;
                $scope.postsCliente.telefono = data.telefono;
                $scope.postsCliente.nombreDueno = data.nombreDueno;
                $scope.postsCliente.id = data.id;
                $scope.clienteUsuario = data;                                
            })
            .error(function (err) {
                alert("Ha fallado la petición HTTP " + err);
            });
     
    $scope.postsRol = [];   
    $http.get("./rest/rolcliente/rolusuario/" + $stateParams.id + "/")
            .success(function (data) {                
                $scope.postsRol = data;
            })
            .error(function (err) {
                alert("Ha fallado la petición HTTP " + err);
            });

    $scope.postsGrupo = [];
 
    $http.get("./rest/grupo/" + $stateParams.id)
            .success(function (data) {
                //console.log(data);
                $scope.postsGrupo.nombre = data.nombre;
            })
            .error(function (err) {
                alert("Ha fallado la petición HTTP " + err);
            });

    $scope.postsPuntos = [];
    //console.log("./rest/punto/?idCliente="+$scope.id);
    $http.get("./rest/punto/?idCliente="+$scope.id)
            .success(function (data) {

                //console.log(data);
                $scope.postsPuntos = data;


            })
            .error(function (err) {

                alert("Ha fallado la petición HTTP " + err);
            });
    //console.log($scope.id);
    
    $scope.processFormUsuario = function () {
        //console.log('processForm desde editar :)');
        //console.log($stateParams.id);
        $http.put('./rest/punto/'+$stateParams.id,$scope.clienteUsuario);
//        $http.put('./rest/user/'+$stateParams, {
//            'email': $scope.posts.email,
//            'fullName': $scope.posts.fullName,
//            'id': $stateParams.id,
//            'idCliente': $scope.posts.idCliente,
//            'idGrupo': null,
//            'password': '',
//            'username': $scope.posts.email
//        }).success(function (data) {
//                    //console.log(id);
//                    //console.log(data);
//                    console.log(data);
//                    //console.log($scope.postsCliente.id);
//                    $scope.message = "El Usuario se ha modificado satisfactoriamente!";
//                    $scope.ruta = "#/usuarios";
//                    $scope.tipo = "alert-success";
//                    //console.log('/rest/rolcliente/userrol/?id_user='+data+'&id_rolcliente='+1); //preguntar de donde sale este 1
//                    $http.put('/rest/rolcliente/userrol/?id_user=' + $stateParams.id + '&id_rolcliente=1');
//        }).error(function (data) {
//            //console.log(data);
//            //console.log("Se quedo en el camino del punto");
//            //console.log(data);
//            $scope.message = "El Usuario se ha modificado satisfactoriamente!";
//            $scope.ruta = "#/usuarios";
//            $scope.tipo = "alert-warning";
//        });
    };
});

//****************************Fin Controladores para procesar información de Usuarios********************************************************************


//****************************Controladores para procesar información de Roles***************************************************************************
angularRoutingApp.controller('rolesController', function ($scope, $http) {
    $scope.posts = [];
    $http.get("./rest/rol/")
            .success(function (data) {

                console.log(data);
                $scope.posts = data;
            })
            .error(function (err) {

                alert("Ha fallado la petición HTTP " + err);
            });
});

angularRoutingApp.controller('crearrolController', function ($scope, $http) {
    $scope.posts = [];
    $scope.formData = {};
    $http.get("./rest/rolcliente/transacciones/")
            .success(function (data) {

                console.log(data);
                $scope.posts = data;
            })
            .error(function (err) {

                alert("Ha fallado la petición HTTP " + err);
            });
            $scope.processFormRol = function() {
               console.log($scope.formData); 
               console.log($scope.formData.id); 
               console.log($scope.formData.nombre);
               if($scope.formData.id!==undefined){
               
               $http.post('./rest/rolcliente/',
                {
                   'activo': 1,
                   'id_cliente': 1,
                   'id_rol': 1,
                   'nombre': $scope.formData.nombre

                })
                .success(function (data) {
                  
                  $scope.message="El Rol se ha creado satisfactoriamente";
                   $scope.ruta="#/roles";
                   $scope.tipo="alert-success";
                console.log(data);
            
  
                    
    }).error(function (data) {
                  
                  $scope.message="No se creo el Rol porque el servicio de Roles esta Roto";
                   $scope.ruta="roles";
                   $scope.tipo="alert-danger";
                console.log(data);
            
  
                    
    });
               }else{
                   $scope.message="Se debe seleccionar al menos un permiso para Crear El Rol";
                   $scope.ruta="crearrol";
                   $scope.tipo="alert-warning";
               }
};

});
angularRoutingApp.controller('listarolesinactivosController', function ($scope, $http) {
    $scope.posts = [];
    $http.get("./rest/rol/inactivos/")
            .success(function (data) {

                console.log(data);
                $scope.posts = data;    
            })
            .error(function (err) {

                alert("Ha fallado la petición HTTP " + err);
            });
});
angularRoutingApp.controller('buscarrolController', function ($scope, $http, $stateParams) {
    $scope.posts = {};
    console.log("./rest/rolcliente/"+$stateParams.id);
    $http.get("./rest/rolcliente/"+$stateParams.id)
            .success(function (data) {

                console.log(data);
                $scope.posts.nombre = data.nombre;
                $scope.posts.roles = data.roles;
            })
            .error(function (err) {

                alert("Ha fallado la petición HTTP " + err);
            });
});



//****************************Fin Controladores para procesar información de Roles***********************************************************************


//****************************Controladores para procesar información de Articulos***********************************************************************
angularRoutingApp.controller('articulosController', function ($scope, $http) {
     $scope.posts = [];

       $scope.inventario = true;
       $scope.venta = true;
       $scope.receta=true;
       $scope.empaques=true;
       //$scope.btn=['inventario','venta','receta','empaques','guardar'];
       
    
   
    
    $http.get("./rest/articulo/?mostrar=activos/")
            .success(function (data) {

                console.log(data);
                $scope.posts = data;
            })
            .error(function (err) {

                alert("Ha fallado la petición HTTP " + err);
            });
            
            
//      $scope.formVisibility = false;
//      $scope.Showform = function () {
//          
//          
//
//         $scope.formVisibility = true;
//         console.log($scope.formVisibility)
//    }; 
    
    /** esta funcion se llamas desde el link y ella va mostrar y ocultar*/
        $scope.habilitar  = function () {
        console.log('activando inventario');
        console.log('ruta inventario');
        $scope.rutainv = "creararticulo.inventarioarticulo";
        $scope.inventario = !$scope.inventario;
    };
    
      $scope.activar = function(){
       console.log('activando ventas')   
       $scope.venta =!$scope.venta;
       console.log('ruta ventas');
        $scope.rutaven= "creararticulo.ventas";
        
    };
    
       $scope.seleccionar = function(){
       console.log('activando ventas')   
       $scope.receta =!$scope.receta;
        
    };
    
    
       $scope.adicionar = function(){
       console.log('activando ventas')   
       $scope.empaques =!$scope.empaques;
        
    };
    
        $scope.checkboxModel = function() {
        console.log('estoy activando')
        };
    //     $scope.pasaruta = function(){
    //     btn =['inventario','venta','receta','empaques','guardar']; 
    //     var rutas =1;
    //    
    //      arr.push(function(){
    //      
    //        console.log(rutas);  
    //    });
    //  
                   
});




 


angularRoutingApp.controller('listaarticulosinactivosController', function ($scope, $http) {
    $scope.posts = [];
    $http.get("./rest/articulo/inactivos/")
            .success(function (data) {

                console.log(data);
                $scope.posts = data;
            })
            .error(function (err) {

                alert("Ha fallado la petición HTTP " + err);
            });
});

angularRoutingApp.controller('inactivararticuloController', function ($scope, $http, $stateParams) {

    $scope.posts = [];
    $http.delete("./rest/articulo/"+$stateParams.id+"/inactivar")
            .success(function (data) {

                console.log(data);
                $scope.posts = data;
            })
            .error(function (err) {

                alert("Ha fallado la petición HTTP " + err);
            });
    

});




angularRoutingApp.controller('buscararticuloController', function ($scope, $http, $stateParams) {

    $scope.posts = {};
    console.log("./rest/articulo/"+$stateParams.id);
    $http.get("./rest/articulo/"+$stateParams.id)
            .success(function (data) {

                console.log(data);
                $scope.posts.id = data.id;
                $scope.posts.idCliente = data.idCliente;
                $scope.posts.nombre = data.nombre;
                $scope.posts.codigo = data.codigo;
                $scope.posts.costoEstimado = data.costoEstimado;                
                 
                $scope.posts.puntos = data.puntos;
                $scope.posts.maximosMinimos = data.inventarioEnt.maximosMinimos;
                $scope.posts.unidadesCompra = data.inventarioEnt.unidadesCompra;
                $scope.posts.unidadesPrincipal = data.ventaEnt.unidadesVenta;
                $scope.posts.gruposSeleccion = data.ventaEnt.gruposSeleccion;
                $scope.posts.recetas = data.recetas;
                
                if(data.seleccion===true){
                   $scope.posts.seleccion = data.seleccion;
                   document.getElementById("seleccion").checked = true;
                }
                
                if(data.utilizaEmpaque===true){
                   $scope.posts.utilizaEmpaque = data.utilizaEmpaque;
                   document.getElementById("empaque").checked = true;
                }
                
                if(data.costoEstimado!==null){
                   document.getElementById("costo").checked = true;
                   $scope.posts.costoEstimado = data.costoEstimado;
                }
                
                if(data.inventario===true){
                   $scope.posts.inventario = data.inventario;
                   document.getElementById("inventario").checked = true;
                }
                
                if(data.venta===true){
                   $scope.posts.utilizaEmpaque = data.venta;
                   document.getElementById("venta").checked = true;
                }
                
                if(data.receta===true){
                   $scope.posts.receta = data.receta;
                   document.getElementById("receta").checked = true;
               }
            })
            .error(function (err) {

                alert("Ha fallado la petición HTTP " + err);
            });
            
            $scope.postsBodegas = {};
    console.log("./rest/bodegas/?idArticulo="+$stateParams.id);
    $http.get("./rest/bodegas/?idArticulo="+$stateParams.id)
            .success(function (data) {

                console.log(data);
                $scope.postsBodegas = data;
                
            })
            .error(function (err) {

                alert("Ha fallado la petición HTTP " + err);
            });
            
            
            $scope.postsCanales = {};
            var id=1;
    console.log("./rest/cliente/"+id+"/canales/");
    $http.get("./rest/cliente/"+id+"/canales/")
            .success(function (data) {

                console.log(data);
                $scope.postsCanales = data;
                
            })
            .error(function (err) {

                alert("Ha fallado la petición HTTP " + err);
            });

});

angularRoutingApp.controller('activararticuloController', function ($scope, $http, $stateParams) {

    $scope.posts = [];
    $http.put("./rest/articulo/"+$stateParams.id+"/activar")
            .success(function (data) {

                console.log(data);
                $scope.posts = data;
                alert("******************creo que lo inactivó*************************** ");
            })
            .error(function (err) {

                alert("Ha fallado la petición HTTP " + err);
            });
            
    

});

//****************************Fin Controladores para procesar información de Articulos*******************************************************************

//****************************Controladores para procesar información de Grupos de Selección*************************************************************
angularRoutingApp.controller('gruposdeseleccionController', function ($scope, $http) {
    $scope.posts = [];
    $http.get("./rest/grupoSeleccion/?mostrar=activos")
            .success(function (data) {

                console.log(data);
                $scope.posts = data;
            })
            .error(function (err) {

                alert("Ha fallado la petición HTTP " + err);
            });
});

angularRoutingApp.controller('listagruposeleccioninactivosController', function ($scope, $http) {
    $scope.posts = [];
    $http.get("./rest/grupoSeleccion/?mostrar=inactivos/")
            .success(function (data) {

                console.log(data);

                $scope.posts = data;
            })
            .error(function (err) {

                alert("Ha fallado la petición HTTP " + err);
            });
});

angularRoutingApp.controller('creargruposdeseleccionController', function ($scope, $http) {
    $scope.posts = [];
       $scope.formData= {
        name: 'caracteristicasPrecio'
      };
               
            
     
      
    $http.get("./rest/articulo/?mostrar=activos/")
            .success(function (data) {

                console.log(data);
                $scope.posts = data;
            })
            .error(function (err) {

                alert("Ha fallado la petición HTTP " + err);
            });
});

angularRoutingApp.controller('buscargruposeleccionController', function ($scope, $http, $stateParams) {
    $scope.posts = [];
    console.log("./rest/grupoSeleccion/"+$stateParams.id);
    $http.get("./rest/grupoSeleccion/"+$stateParams.id)
            .success(function (data) {

                console.log(data);
                $scope.posts.nombre = data.nombre;
                $scope.posts.articulos = data.articulos;
            })
            .error(function (err) {

                alert("Ha fallado la petición HTTP " + err);
            });
            
            $scope.seccion = [];
            console.log("./rest/grupoSeleccion/"+$stateParams.id+"/selecciones/");
            $http.get("./rest/grupoSeleccion/"+$stateParams.id+"/selecciones/")
            .success(function (datasec) {

                console.log(datasec);
                $scope.seccion = datasec;
            })
            .error(function (err) {

                alert("Ha fallado la petición HTTP " + err);
            });
});


angularRoutingApp.controller('creargruposeleccionController', function ($scope, $http, localStorageService) {
    $scope.posts = [];
    $scope.formData=[];
    
    $scope.noAplica = true;
  
    $scope.processFormGrupoSeleccion = function() {
                
               console.log($scope.formData); 
               if($scope.formData.incrementoPrecio===undefined){
                   $scope.formData.incrementoPrecio="";
               }
               
           console.log($scope.formData.incrementoPrecio); 
           
            $http.post('./rest/grupoSeleccion/',
                {
                   'activo': null,
                   'caracteristicaPrecio': $scope.formData.caracteristicasPrecio,
                   'id': null,
                   'incrementoPrecio': $scope.formData.incrementoPrecio,
                   'nombre': $scope.formData.nombre,
                   'tipoGrupo': $scope.formData.tipoGrupo

                })
                .success(function (data) {
                    
                  
                  
                console.log(data);
                console.log("./rest/grupoSeleccion/"+data+"/selecciones/");
                console.log("///////////////////////////");
                $scope.grupos=localStorageService.get("angular-grupos",$scope.grupos);
                console.log($scope.grupos);
                $http.post('./rest/grupoSeleccion/'+data+'/selecciones/',$scope.grupos);               
  
                    
    });
};

    $scope.removeArt = function(id, kardex) {                
        $scope.articulos = articulos;
        console.log($scope.articulos.length);
        var index;
      for(var n = 0; $scope.articulos.length; n ++){
          console.log($scope.articulos[n]);                     
          if($scope.articulos[n].idArticulo === id && $scope.articulos[n].cantidadADescargar === kardex){
                index = $scope.articulos.indexOf($scope.articulos);
                $scope.articulos.splice(index, 1);                
          };
          break;
      }
      
      $scope.myDataArticulos =  $scope.articulos;
      $scope.grupos=localStorageService.set("angular-myDataArticulos", $scope.articulos);           
      console.log(id + ' --- ' + kardex);
    };
    
    
});
//****************************Fin Controladores para procesar información de Grupos de Selección*********************************************************

//****************************Controladores para procesar información de Lista de Precios****************************************************************
angularRoutingApp.controller('listapreciosController', function ($scope, $http) {
    $scope.posts = [];
    var id=1;
    $http.get("./rest/cliente/"+id+"/listasPrecios")
            .success(function (data) {

                console.log(data);
                $scope.posts = data;
            })
            .error(function (err) {

                alert("Ha fallado la petición HTTP " + err);
            });
});

angularRoutingApp.controller('listapreciosinactivosController', function ($scope, $http) {
    $scope.posts = [];
    var id=1;
    $http.get("./rest/cliente/"+id+"/listasPrecios?mostrar=inactivos")
            .success(function (data) {

                console.log(data);
                $scope.posts = data;
            })
            .error(function (err) {

                alert("Ha fallado la petición HTTP " + err);
            });
});



angularRoutingApp.controller('crearlistapreciosController', function ($scope, $http, $notify) {
    $scope.formData = {};
    $scope.processFormListaPrecios = function() {
        console.log("enviando datos ");
    console.log($scope.formData);
    // ////////////////////////////Acuerdate de quitar el 1/////////////////////////////////////////////
    $http.post("./rest/cliente/"+1+"/listasPrecios",$scope.formData)
            .success(function (data){
                   $scope.message="La Lista de Precios se ha creado satisfactoriamente";
                   $scope.ruta="#/listaprecios";
                   $scope.tipo="alert-success";
            }).error(function(data){
                $scope.message=" Fallo al crearLa Lista de Precios";
                   $scope.ruta="#/crearlistaprecios";
                   $scope.tipo="alert-info";
            }); 
        
    };
});
//****************************Fin Controladores para procesar información de Listas de Precios***********************************************************

//****************************Controladores para procesar información de Listas de Clases****************************************************************
angularRoutingApp.controller('clasesController', function ($scope, $http) {
    $scope.posts = [];
    $http.get("./rest/cliente/clase/")
            .success(function (data) {

                console.log(data);
                $scope.posts = data;
            })
            .error(function (err) {

                alert("Ha fallado la petición HTTP " + err);
            });
});

angularRoutingApp.controller('listaclasesinactivasController', function ($scope, $http) {
    $scope.posts = [];
    $http.get("./rest/cliente/clase/?mostrar=inactivos")
            .success(function (data) {

                console.log(data);
                $scope.posts = data;
            })
            .error(function (err) {

                alert("Ha fallado la petición HTTP " + err);
            });
});

angularRoutingApp.controller('buscarclaseController', function ($scope, $http, $stateParams) {
    $scope.posts = {};
    console.log("./rest/cliente/clase/"+$stateParams.id);
    $http.get("./rest/cliente/clase/"+$stateParams.id)
            .success(function (data) {

                console.log(data);
                $scope.posts.nombre = data.nombre;
                $scope.posts.grupos = data.grupos;
                
                if(data.inventario===true){
                   document.getElementById("usadainventario").checked = true;
                   
                }
                if(data.venta===true){
                   document.getElementById("unidadventa").checked = true;
                   
                }
                
                
            })
            .error(function (err) {

                alert("Ha fallado la petición HTTP " + err);
            });
});


angularRoutingApp.controller('crearclasesController', function ($scope, $http, localStorageService) {
    $scope.formData = {};
    $scope.processFormClases = function() {
       $scope.myDataClase= localStorageService.get("angular-myDataClase",$scope.myDataClase);
       console.log("enviando datos ");
        console.log($scope.formData);
        console.log($scope.myDataClase);
        
        $http.post("./rest/cliente/clase/",{
            activo :null,
            grupos :$scope.myDataClase,
            id :null,
            inventario :$scope.formData.inventario,
            nombre :$scope.formData.nombre,
            venta :$scope.formData.venta
        })
        .success(function (data){
              console.log("envio los datos");
              $scope.message="La Clase se ha creado satisfactoriamente";
                   $scope.ruta="#/clases";
                   $scope.tipo="alert-success";
                   localStorage.clear();
                
            localStorage.clear();
            }).error(function(data){
                console.log("Se quedo en el camino"); 
                $scope.message="Fallo al crear la Clase favor completar los campos, flojo!!!!";
                   $scope.ruta="#/clases";
                   $scope.tipo="alert-danger";
                   localStorage.clear();
            });
       

   /* $http.post("./rest/cliente/clase/",$scope.formData)
            .success(function (data){
              console.log("envio los datos");
            localStorage.clear();
            }).error(function(data){
                console.log("Se quedo en el camino"); 
                localStorage.clear();
            }); */
    };
});
//****************************Fin Controladores para procesar información de Listas de Clases***********************************************************

//****************************Controladores para procesar información de Puntos*************************************************************************

angularRoutingApp.controller('puntosController', function ($scope, $resource) {
    Post = $resource("http://localhost/json/prueba.json");
    $scope.posts = Post.query();
});
angularRoutingApp.controller('listapuntosinactivosController', function ($scope, $resource) {
    Post = $resource("http://localhost/json/prueba.json");
    $scope.posts = Post.query();
});
angularRoutingApp.controller('crearpuntoController', function ($scope, $stateParams) {
    $scope.posts = $stateParams.id;
    $scope.cond = $stateParams.cond;
    $scope.guardar = function() {
        console.log($scope.formData.pais);
        $scope.punto = {};
        $scope.punto.nombre = $scope.formData.nombre;
        $scope.punto.direccion = $scope.formData.direccion;
        $scope.punto.telefono = $scope.formData.telefono;
        $scope.punto.extension = $scope.formData.extension;
        $scope.punto.telefono2 = $scope.formData.telefono2;
        $scope.punto.extension2 = $scope.formData.extension2;
        $scope.punto.costo = $scope.formData.centro_costo_id;
        $scope.punto.negocio = $scope.formData.negocio;
        $scope.punto.pais = $scope.formData.pais.nombre;
        $scope.punto.departamento = $scope.formData.departamento;
        $scope.punto.municipio = $scope.formData.municipio;
        console.log($scope.punto);
    };
});

angularRoutingApp.controller('buscarpuntosclienteController', function ($scope, $http, $stateParams) {

    $scope.posts = [];
    $http.get("./rest/punto/?idCliente="+$stateParams.id)
            .success(function (data) {

                console.log(data);
                $scope.posts = data;


            })
            .error(function (err) {

                alert("Ha fallado la petición HTTP " + err);
            });

});

angularRoutingApp.controller('buscarpuntosarticuloController', function ($scope, $http, $stateParams) {

    $scope.posts = [];
    $http.get("./rest/articulo/"+$stateParams.id+"/puntos/")
            .success(function (data) {

                console.log(data);
                $scope.posts = data;


            })
            .error(function (err) {

                alert("Ha fallado la petición HTTP " + err);
            });

});


angularRoutingApp.controller('buscarpuntosespecificosController', function ($scope, $http, $stateParams) {
    
    $scope.posts = {};
    $scope.post={};
    $scope.postConf={};
    $scope.post= $stateParams.id;
   console.log("./rest/punto/"+$stateParams.id);
    $http.get("./rest/punto/"+$stateParams.id)
    
            .success(function (data) {
 
                console.log(data);
                $scope.posts.id = data.id;
                $scope.posts.nombre = data.nombre;
                $scope.posts.direccion = data.direccion;
                $scope.posts.telefono = data.telefono;
                $scope.posts.extension = data.extension;
                $scope.posts.telefono = data.telefono;
                $scope.posts.extension = data.extension;
                $scope.posts.telefono2 = data.telefono2;
                $scope.posts.extension2 = data.extension2;
                $scope.posts.municipio_id = data.municipio_id;
                $scope.posts.tipo_negocio_id = data.tipo_negocio_id;
                $scope.posts.centro_costo_id = data.centro_costo_id;
                $scope.posts.cliente_id = data.cliente_id;
                $scope.posts.activo = data.activo;
                $scope.posts.marcas = data.marcas;
                $scope.posts.bodegas = data.bodegas;
                
                if (data.almacenarNpunto===true){
                   $scope.postConf.almacenarNpunto = "Permite almacenar Puntos";                    
                }
                
                if (data.apoyalicrec===true){
                   $scope.postConf.apoyalicrec = "Permite apoyo en licrec";                    
                }
                
                if (data.autAnularVerificacion===true){
                   $scope.postConf.autAnularVerificacion = "Permite autenticar y anular Verificacion";                    
                }
                
                if (data.autoUsuEliminar===true){
                   $scope.postConf.autoUsuEliminar = "Permite auto eliminar usuarios";                    
                }
                
                if (data.capCodAutoserv===true){
                   $scope.postConf.capCodAutoserv = "Permite capturar codigo de autoservicio";                    
                }
                
                if (data.capCodmesero===true){
                   $scope.postConf.capCodmesero = "Permite capturar el codigo Del mesero";                    
                }
                
                if (data.capNpersonasAt===true){
                   $scope.postConf.capNpersonasAt = "Permite capturar el numero de personas atendidas";                    
                }
                
                if (data.cierreocultocaja===true){
                   $scope.postConf.cierreocultocaja = "Permite el cierre oculto de la caja";                    
                }
                
                if (data.claveanular===true){
                   $scope.postConf.claveanular = "Permite anular la clave";                    
                }
                if (data.claveanularVerifi===true){
                   $scope.postConf.claveanularVerifi = "Permite anular la clave de verificacion";                    
                }
                
                if (data.contXdenominacion===true){
                   $scope.postConf.contXdenominacion = "Permite contar por denominacion";                    
                }
                
                if (data.controlconspalabra===true){
                   $scope.postConf.controlconspalabra = "Permite el control con palabra";                    
                }
                
                if (data.denoatendidopor===true){
                   $scope.postConf.denoatendidopor = "Permite no se que atendido por";                    
                }
                
                if (data.factFresumido===true){
                   $scope.postConf.factFresumido = "Permite facturar fresumido";                    
                }
                
                if (data.factNpunto===true){
                   $scope.postConf.factNpunto = "Permite facturar numero de punto";                    
                }
                
                if (data.factSinDocVerif===true){
                   $scope.postConf.factSinDocVerif = "Permite facturar sin verificar documento";                    
                }
                
                if (data.fvmenseros===true){
                   $scope.postConf.fvmenseros = "Permite fv meseros";                    
                }
                
                if (data.impremotacpfactura===true){
                   $scope.postConf.impremotacpfactura = "Permite imprimir remotamente la factura";                    
                }
                
                if (data.impticketAutoSrv===true){
                   $scope.postConf.impticketAutoSrv = "Permite imprimir ticket de autoservicios";                    
                }
                
                if (data.inclivaPventa===true){
                   $scope.postConf.inclivaPventa = "Incluye iva punto de venta";                    
                }
                
                if (data.manNlicencias===true){
                   $scope.postConf.manNlicencias = "Maneja numero de licencias";                    
                }
                
                if (data.opLicNred===true){
                   $scope.postConf.opLicNred = "Opcion licencias de red";                    
                }

            })
            .error(function (err) {
                console.log(err);
            });
});




// our controller for the form
// =============================================================================


angularRoutingApp.controller('formclienteController', function ($scope, $timeout, $location, $http, localStorageService) {
    $scope.formData = {};
    $scope.posts = {};


    $scope.processFormCliente = function() {
        console.log("$Valor de todo el formulario Crear Clientes");
        console.log($scope.formData);

       


    $http.post('./rest/cliente/',
    {
       'id': null,
       'nombre': $scope.formData.nombre,
       'nit': $scope.formData.nit,
       'digitoChequeo': $scope.formData.digitoChequeo,
       'direccion': $scope.formData.direccion,
       'telefono': $scope.formData.telefono,
       'extension': $scope.formData.extension,
       'email': $scope.formData.email,
       'celular': $scope.formData.celular,
       'regimen_id': null,
       'nombreRepresentante': $scope.formData.nombreRepresentante,
       'documentoRepresentante': $scope.formData.documentoRepresentante,    
       'nombreDueno': $scope.formData.nombreDueno,
       'mailDueno': $scope.formData.mailDueno,
       'celularDueno': $scope.formData.celularDueno,
       'municipio': $scope.formData.municipio.id

  
    })
            .success(function (data){
              console.log("envio los datos cliente");  
      
              console.log("./rest/cliente/busqueda/"+$scope.formData.nombre);
                $http.get("./rest/cliente/busqueda/"+$scope.formData.nombre)
    
            .success(function (data) {
               $scope.posts = data;
               angular.forEach($scope.posts, function(id) {
                console.log(id.id);
                console.log("./rest/cliente/"+id.id+"/regimenes/");
               
                $scope.myDataId=localStorageService.get("angular-myDataId",$scope.myDataId);
                 $http.post("./rest/cliente/"+id.id+"/regimenes/",$scope.myDataId);
       
       
               console.log("./rest/cliente/"+id.id+"/configuracion/");
               $http.post("./rest/cliente/"+id.id+"/configuracion/",
               {
                'empaqueDiferencialXCanal': $scope.formData.empaqueporcanal,
                'empaqueDiferencialXPunto': $scope.formData.empaqueporpunto,
                'recetaDiferencialXCanal': $scope.formData.recetaporcanal,
                'recetaDiferencialXPunto': $scope.formData.recetaporpunto,
                'unicoImpuestoXCanales': false
               });

                console.log("./rest/cliente/"+id.id+"/canalImpuesto/");
                if($scope.formData.canal==="AutoServicio"){
                    $scope.formData.canal = 1;
                 }
                 if($scope.formData.canal==="Para Llevar"){
                     $scope.formData.canal = 2;
                 }
                 if($scope.formData.canal==="Domicilio"){
                     $scope.formData.canal = 3;
                 }
                 if($scope.formData.impuesto2==="Iva"){
                     $scope.formData.impuesto2 = 1;
                 }
                 if($scope.formData.impuesto2==="Ipoconsumo"){
                     $scope.formData.impuesto2 = 2;
                 }
                 if($scope.formData.impuesto2==="Especial"){
                     $scope.formData.impuesto2 = 3;
                 }
                 $http.post("./rest/cliente/"+id.id+"/canalImpuesto/",{
                   
                'idCanal': $scope.formData.canal,
                'idCliente': id.id,
                'idImpuesto': $scope.formData.impuesto2,
                'idRegimen': 5,//lo trae de la lista de impuesto
                'idTarifa': 8,//lo trae del checkbox tarifa unica por canal
                'predeterminado': true  

                });
                

                 

                   $scope.showMessage = true;
                   $timeout(function() {
                       $scope.showMessage = false;
                       $location.path('/clientes');
                   },  3000);
                   
                   $scope.message="El Cliente se ha creado satisfactoriamente";
                   $scope.ruta="#/clientes";
                   $scope.tipo="alert-success";
                        localStorage.clear();
                        //window.location.href ="http://localhost:8080/sab/main#/clientes";
        });

}); 
      
            }).error(function(data){
                console.log("Se quedo en el camino cliente"); 
                                   $scope.showMessage = true;
                   $timeout(function() {
                       $scope.showMessage = false;
                      // $location.path('/canalesimp');
                   },  3000);
              $scope.message="Fallo al crear Cliente datos erroneos en los formularios";
                   $scope.ruta="#/clientes";
                   $scope.tipo="alert-danger";
                   localStorage.clear();
                        //window.location.href ="http://localhost:8080/sab/main#/clientes";
            });

        

    };


});

//**************************************************************************************************************************
angularRoutingApp.controller('FormUsuarioEspecifico', function ($scope, $http) {
    $scope.formData = {};
    $scope.processFormUsuarioEspecifico = function() {
        console.log("enviando datos ");
    console.log($scope.formData);
    
    $http.post("./rest/cliente/",$scope.formData)
            .success(function (data){
              console.log("envio los datos usuario");  
            }).error(function(data){
                console.log("Se quedo en el camino usuario"); 
            }); 
        
    };
});

//***************** controlador form usuario, crear ******************************
angularRoutingApp.controller('formusuarioController', function ($scope,$timeout, $http, $location) {


    $scope.currentPage = 5;
    $scope.pageSize = 5;


    $scope.posts = [];
    $http.get("./rest/cliente/")
            .success(function (data) {

                console.log(data);
                $scope.posts = data;
            })
            .error(function (err) {

                alert("Ha fallado la petición HTTP " + err);
            });


    $scope.grupos = [];
    $http.get("./rest/grupo/")
            .success(function (data) {

                console.log(data);
                $scope.grupos = data;
            })
            .error(function (err) {

                console.log("Ha fallado la petición HTTP ");
            });


    $scope.puntos = [];
    $http.get("./rest/punto/?idCliente=1")//solo esta mostrando los puntos de gamasoft*****************************************************
            .success(function (data) {

                console.log(data);
                $scope.puntos = data;


            })
            .error(function (err) {

                alert("Ha fallado la petición HTTP " + err);
            });


    $scope.roles = [];
    $http.get("./rest/rol/")
            .success(function (data) {

                console.log(data);
                $scope.roles = data;
            })
            .error(function (err) {

                console.log("Ha fallado la petición HTTP ");
            });


    $scope.formData = {};
    $scope.formDataRol = {};
    $scope.processFormUsuario = function () {
        console.log("Enviando datos del usuario");
        console.log($scope.formData);
        $http.post('./rest/user/',{
                'email':$scope.formData.email,
                'fullName':$scope.formData.fullName,
                'id':null,
                'idCliente':$scope.formData.idCliente,
                'idGrupo':null,
                'password':$scope.formData.password,
                'username':$scope.formData.email
                })
              .success(function (data){
              console.log(data);
      
              console.log($scope.formData.id);
              $scope.message="El Usuario se ha creado satisfactoriamente pero ";
                   $scope.ruta="#/usuarios";
                   $scope.tipo="alert-success";
              console.log('/rest/rolcliente/userrol/?id_user='+data+'&id_rolcliente='+1); //preguntar de donde sale este 1
              $http.post('/rest/rolcliente/userrol/?id_user='+data+'&id_rolcliente=1',$scope.formData.id); 
            }).error(function(data){
                console.log("Se quedo en el camino del punto"); 
                $scope.message="El Usuario se ha creado satisfactoriamente pero falla en el rol por el WS q esta roto";
                   $scope.ruta="#/usuarios";
                   $scope.tipo="alert-warning";
            }); 
    };
    
});

                    console.log($scope.formData.id);
                    $scope.message = "El Usuario se ha creado satisfactoriamente pero falla en el rol por el WS q esta roto. sera enviado a lista de usuarios.";
                    $scope.ruta = "#/usuarios";
                    $scope.tipo = "alert-success";
                    console.log('/rest/rolcliente/userrol/?id_user=' + data + '&id_rolcliente=' + 1); //preguntar de donde sale este 1
                    $http.post('/rest/rolcliente/userrol/?id_user=' + data + '&id_rolcliente=1', $scope.formData.id);
                    //funcion mensaje alert 3 segundos
                    $scope.alert = true;                    
                    $timeout(function () {
                        $scope.alert = false;
                        $location.path('/usuarios');
                    },3000
                    )
                    
             .error(function (data) {
            console.log("Se quedo en el camino del punto");
            $scope.message = "El Usuario se ha creado satisfactoriamente pero falla en el rol por el WS q esta roto";
            $scope.ruta = "#/usuarios";
            $scope.tipo = "alert-warning";
        });

//**************************************************************************************
angularRoutingApp.controller('formarticuloController', function($scope) {
    
    
    $scope.formData = {};
    
   
    $scope.processFormArticulo = function() {
        alert('Buscar ruta para guardar articulo!');
        console.log($scope.formData);
    };

});




angularRoutingApp.controller('formpuntoController', function($scope, $http, $stateParams, localStorageService) {

    $scope.formData = {};

    $scope.postid = $stateParams.id;
    $scope.posts = {};
    $scope.postsMarcas = [];
    $scope.postsBodegas = [];
    
    

//    
//    $http.get("./rest/marca/")
//            .success(function (data) {
//
//                console.log(data);
//                $scope.postsMarcas = data;
//            })
//            .error(function (err) {
//
//                alert("Ha fallado la petición HTTP " + err);
//            });
//            
//            
//            $http.get("./rest/bodegas/")
//            .success(function (data) {
//
//                console.log(data);
//                $scope.postsBodegas = data;
//            })
//            .error(function (err) {
//
//                alert("Ha fallado la petición HTTP " + err);
//            });

   // $scope.posts=$stateParams.id;
   // console.log("valor id "+$scope.posts);
    
    $scope.processFormPunto = function() {

        $scope.conf = localStorageService.get("angular-conf",$scope.conf);
        $scope.postsMarcas=localStorageService.get("angular-postsMarcas",$scope.postsMarcas);
        $scope.postsBodegas=localStorageService.get("angular-postsBodegas",$scope.postsBodegas);
        
        console.log("valor del id "+$scope.formData.idCliente);        
        console.log("valor del nombre "+$scope.formData.nombre);
        console.log("valor del direccion "+$scope.formData.direccion);
        console.log("valor del telefono "+$scope.formData.telefono);
        console.log("valor del extension "+$scope.formData.extension);
        console.log("valor del telefono2 "+$scope.formData.telefono2);
        console.log("valor del extension2 "+$scope.formData.extension2);
        console.log("valor del centroCosto "+$scope.formData.centro_costo_id);
        console.log("valor del tipoNegocio "+$scope.formData.tipo_negocio_id);
        //console.log("valor del pais "      +$scope.formData.pais);
       // console.log("valor del departamento "+$scope.formData.departamento);
        //console.log("valor del municipio"   +$scope.formData.municipio);
       
        
        console.log("///////////////////////////////////");
        console.log($scope.formData);
        console.log($scope.conf);
        console.log($scope.postsMarcas);
        
        
        
        
       console.log($scope.postsBodegas);
        
        
      

    $http.post('./rest/punto/',
    {
        'activo':true,
        'almacenarNpunto':false,
        'apoyalicrec':false,
        'autAnularVerificacion':false,
        'autoUsuEliminar':false,
        'capCodAutoserv':false,
        'capCodmesero':false,
        'capNpersonasAt':false,
        'centro_costo_id':null,
        'cierreocultocaja':false,
        'claveanular':false,
        'claveanularVerifi':false,
        'cliente_id':$scope.formData.idCliente,
        'contXdenominacion':false,
        'controlconspalabra':false,
        'denoatendidopor':false,
        'direccion':$scope.formData.direccion,
        'extension':$scope.formData.extension,
        'extension2':$scope.formData.extension2,
        'factFresumido':false,
        'factNpunto':false,
        'factSinDocVerif':false,
        'fvmenseros':false,
        'id':null,
        'imprRemotas':false,
        'impremotacpfactura':false,
        'impticketAutoSrv':false,
        'inclivaPventa':false,
        'manNlicencias':false,
        'municipio_id':$scope.formData.municipio.id,
        'nombre':$scope.formData.nombre,
        'opLicNred':false,
        'telefono':$scope.formData.telefono,
        'telefono2':$scope.formData.telefono2,
        'tipo_negocio_id':1

    })
            .success(function (data){
              console.log("envio los datos del punto");  
              console.log(data);
       $http.post('./rest/punto/'+data+'/marcas/',$scope.postsMarcas);
       $http.put('./rest/punto/'+data,$scope.conf);
      
                   $scope.message="El Punto se ha creado satisfactoriamente pero falta la configuracion y Bodegas";
                   $scope.ruta="#/clientes";
                   $scope.tipo="alert-success";
            }).error(function(data){
                console.log("Se quedo en el camino del punto");
                 $scope.message="Fallo";
                   $scope.ruta="#/clientes";
                   $scope.tipo="alert-warning";
            }); 
  

    };

});
/*
angularRoutingApp.controller('processFormPuntoEspecifico', function($scope) {
    
    
    $scope.formData = {};
    
   
    $scope.processFormPuntoEspecifico = function() {
        alert('Buscar ruta para guardar punto especifico!');
        console.log($scope.formData);
    };

});
*/
//***********************************************************************************************************************************

angularRoutingApp.controller('cargarlistasController', function ($scope, $http) {
    $scope.postsPais = [];
    $http.get("./rest/common/paises")
            .success(function (data) {

                console.log(data);
                $scope.postsPais = data;
                $scope.formData.pais = "";

            })
            .error(function (err) {

                alert("Ha fallado la petición HTTP " + err);
            });
        
        $scope.cargadepartamento = function(id) {
        console.log("paso por cargar departamento **************************************");
        console.log("Valor del id departamento ruta "+id.id);
        
        
        $scope.postsDepartamento = [];
    $http.get("./rest/common/dptos/?idPais="+id.id)
            .success(function (data) {

                console.log(data);
                $scope.postsDepartamento = data;
                $scope.formData.departamento = "";
            })
            .error(function (err) {

                alert("Ha fallado la petición HTTP " + err);
            });
        
        
};
    $scope.cargamunicipio = function(id) {
        console.log("paso por cargar municipio **************************************");
        console.log("Valor del id municipio ruta "+id.id);
        
        
        $scope.postsMunicipio = [];
    $http.get("./rest/common/municipios/?idDepto="+id.id)
            .success(function (data) {

                console.log(data);
                $scope.postsMunicipio = data;
                $scope.formData.postsMunicipio = "";

            })
            .error(function (err) {

                alert("Ha fallado la petición HTTP " + err);
            });
        
        
};

$scope.cargaimpuesto = function(id) {
        console.log("paso por cargar impuesto **************************************");
        console.log("Valor del id impuesto ruta "+id.id);
        
        
        $scope.postsImpuesto = [];
    $http.get("./rest/common/impuestos/?idPais="+id.id)
            .success(function (data) {

                console.log(data);
                $scope.postsImpuesto = data;
                $scope.formData.postsImpuesto = "";

            })
            .error(function (err) {

                alert("Ha fallado la petición HTTP " + err);
            });
        
        
};

$scope.cargaregimen = function(id) {
        console.log("paso por cargar regimen **************************************");
        console.log("Valor del id regimen ruta "+id.id);
        
        
        $scope.postsRegimen = [];
    $http.get("./rest/common/regimenes/?idImpuesto="+id.id)
            .success(function (data) {

                console.log(data);
                $scope.postsRegimen = data;
                $scope.formData.postsRegimen = "";

            })
            .error(function (err) {

                alert("Ha fallado la petición HTTP " + err);
            });
        
        
};



});



angularRoutingApp.controller('cargarcanalController', function ($scope, $http, localStorageService) {

$scope.btn;
        console.log("paso por cargar canal **************************************");

        $scope.postsCanal = [];
    $http.get("./rest/common/canales")
            .success(function (data) {

                console.log(data);
                $scope.postsCanal = data;
                //$scope.formData.postsCanal = "";

            })
            .error(function (err) {

                alert("Ha fallado la petición HTTP " + err);
            });           
    


    $scope.close = function(result) {
            close(result, 500); // close, but give 500ms for bootstrap to animate
     };


$scope.cargaimpuesto = function(id) {
        console.log("paso por cargar impuesto **************************************");
        console.log("Valor del id impuesto ruta "+id.id);
        
        
        $scope.postsImpuesto = [];
    $http.get("./rest/common/impuestos/?idPais=42")
            .success(function (data) {

                console.log(data);
                $scope.postsImpuesto = data;
                $scope.formData.postsImpuesto = "";

            })
            .error(function (err) {

                alert("Ha fallado la petición HTTP " + err);
            });
        
        
};



$scope.cargaimpuestoselec = function() {
        console.log("paso por cargar impuesto selec **************************************");      

        $scope.ImpuestoRegimen=localStorageService.get("angular-ImpuestoRegimen",$scope.ImpuestoRegimen);
        console.log("valor del $scope.ImpuestoRegimenselec");
        console.log($scope.ImpuestoRegimen);
};




});





angularRoutingApp.controller('cargarlistasClaseController', function ($scope, $http) {


        console.log("paso por cargarlistasClaseController **************************************");

        $scope.postsclaseinventario = [];
    $http.get("./rest/cliente/clase/?inventario=true")
            .success(function (data) {

                console.log(data);
                $scope.postsclaseinventario = data;

            })
            .error(function (err) {

                alert("Ha fallado la petición HTTP " + err);
            });
        
$scope.grupoinventario = function(id) {
        console.log("paso por grupoinventario **************************************");
        console.log("Valor del id  "+id.id);
        
        
        $scope.postsgrupoinventario = [];
    $http.get("./rest/cliente/clase/"+id.id+"/grupos")
            .success(function (data) {

                console.log(data);
                $scope.postsgrupoinventario = data;
                //$scope.formData.postsgrupoinventario = "";

            })
            .error(function (err) {

                alert("Ha fallado la petición HTTP " + err);
            });
        
        
};




});


angularRoutingApp.controller('cargarunidadKardexController', function ($scope, $http) {


  console.log("paso por unidadkardex **************************************");
      
        
        
        $scope.postsunidadkardex = [];
    $http.get("./rest/unidad/")
            .success(function (data) {

                console.log(data);
                $scope.postsunidadkardex = data;


            })
            .error(function (err) {

                alert("Ha fallado la petición HTTP " + err);
            });
            
            
            
            $scope.postsunidadalterna = [];
    $http.get("./rest/unidad/?mostrar=todas/")
            .success(function (data) {

                console.log(data);
                $scope.postsunidadalterna = data;


            })
            .error(function (err) {

                alert("Ha fallado la petición HTTP " + err);
            });

});


angularRoutingApp.controller('comprasController', function ($scope) {
    $scope.posts = [];

});

angularRoutingApp.controller('buscarcompraController', function ($scope) {
    $scope.posts = [];

});
