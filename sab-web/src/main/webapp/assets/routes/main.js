//***************************Creación del módulo**********************************************************************************
var angularRoutingApp = angular.module('angularRoutingApp', [
    'ngRoute', 
    'ngAnimate', 
    'ui.router', 
    'angularModalService', 
    'ngSanitize', 
    'ui.bootstrap', 
    'angularUtils.directives.dirPagination', 
    'ngResource',
    'LocalStorageModule',
    'checklist-model',
    'ngNotify'
    
]);

// =============================================================================

angularRoutingApp.config(function ($stateProvider, $urlRouterProvider) {

    $stateProvider

            .state('/', {
                url: '/',
                templateUrl: 'assets/pages/home.html',
                controller: 'mainController'
            })
//****************************Rutas para procesar información de Clientes**********************************************************           
            .state('/clientes', {
                url: '/clientes',
                templateUrl: 'assets/pages/clientes.html',
                controller: 'clientesController'
            })

            .state('/listaclientesinactivos', {
                url: '/listaclientesinactivos',
                templateUrl: 'assets/pages/listaclientesinactivos.html',
                controller: 'listaclientesinactivosController'
            })
//*****************************************************************************************************************
            .state('buscarcliente', {
                url: '/buscarcliente',
                templateUrl: 'assets/pages/datoscliente.html'
                
            })
            
            .state('buscarcliente.datosbasicosclienteespecifico', {
                url: '/datosbasicosclienteespecifico/:id',
                templateUrl: 'assets/pages/form-datosbasicos-clienteespecifico.html',
                controller: 'buscarclienteController'
            })


            .state('buscarcliente.ajustesclienteclienteespecifico', {
                url: '/ajustesclienteclienteespecifico/:id',
                templateUrl: 'assets/pages/form-ajustescliente-clienteespecifico.html',
                controller: 'buscarAjustesclienteController'
            })


            .state('buscarcliente.canalesimpclienteespecifico', {
                url: '/canalesimpclienteespecifico/:id',
                templateUrl: 'assets/pages/form-canalesimp-clienteespecifico.html',
                controller: 'buscarclienteCanImpController'
            })
//********************************************************************************************************************
            .state('crearclientes', {
                url: '/crearclientes',
                templateUrl: 'assets/pages/crearclientes.html',
                controller: 'formclienteController'
            })
            

            .state('crearclientes.datosbasicos', {
                url: '/datosbasicos',
                templateUrl: 'assets/pages/form-datosbasicos.html',
                controller: 'Controller'
            })


            .state('crearclientes.ajustescliente', {
                url: '/ajustescliente',
                templateUrl: 'assets/pages/form-ajustescliente.html',
                controller: 'Controller'
                
                
            })


            .state('crearclientes.canalesimp', {
                url: '/canalesimp',
                templateUrl: 'assets/pages/form-canalesimp.html',
                controller: 'Controller',
                backdrop: 'static'
            })


//****************************Fin de las rutas para procesar información de Clientes***************************************************  


//****************************Rutas para procesar información de Grupos Corporativos***************************************************  
            .state('gruposcorporativos', {
                url: '/gruposcorporativos',
                templateUrl: 'assets/pages/gruposcorporativos.html',
                controller: 'gruposcorporativosController'
            })

            .state('listagruposcorporativosinactivos', {
                url: '/listagruposcorporativosinactivos',
                templateUrl: 'assets/pages/listagruposcorporativosinactivos.html',
                controller: 'listagruposcorporativosinactivosController'
            })

            .state('/creargrupocorporativo', {
                url: '/creargrupocorporativo',
                templateUrl: 'assets/pages/creargrupocorporativo.html',
                controller: 'creargrupocorporativoController'
            })

            .state('buscargrupo', {
                url: '/buscargrupo/:id',
                templateUrl: 'assets/pages/datosgrupo.html',
                controller: 'buscargrupoController'
            })
//****************************Fin de las rutas para procesar información de Grupos Corporativos****************************************  

            //****************************Rutas para procesar información de Marcas****************************************************************   

            .state('/marcas', {
                url: '/marcas',
                templateUrl: 'assets/pages/marcas.html',
                controller: 'marcasController'
            })
            
            .state('/marcas/:state', {
                url: '/marcas/:state',
                templateUrl: 'assets/pages/marcas.html',
                controller: 'marcasController'
            })

            .state('/crearmarca', {
                url: '/crearmarca',
                templateUrl: 'assets/pages/crearmarca.html',
                controller: 'crearmarcaController'
            })

            .state('/listamarcasinactivas', {
                url: '/listamarcasinactivas',
                templateUrl: 'assets/pages/listamarcasinactivas.html',
                controller: 'listamarcasinactivasController'
            })

            .state('buscarmarca', {
                url: '/buscarmarca/:id',
                templateUrl: 'assets/pages/datosmarca.html',
                controller: 'buscarmarcaController'
            })

//****************************Fin de las rutas para procesar información de Marcas***************************************************** 

//****************************Rutas para procesar información de Usuarios**************************************************************   
            .state('/usuarios', {
                url: '/usuarios',
                templateUrl: 'assets/pages/usuarios.html',
                controller: 'usuariosController'
            })

            .state('/listausuariosinactivos', {
                url: '/listausuariosinactivos',
                templateUrl: 'assets/pages/listausuariosinactivos.html',
                controller: 'listausuariosinactivosController'
            })
   //***************************************************************************************************************************         
            .state('buscarusuario', {
                url: '/buscarusuario',
                templateUrl: 'assets/pages/datosusuario.html'
                
            })  
            
            .state('buscarusuario.datosbasicosusuarioespecifico', {
                url: '/datosbasicosusuarioespecifico/:id',
                templateUrl: 'assets/pages/form-datosbasicos-usuarioespecifico.html',
                controller: 'buscarusuarioController'
            })


            .state('buscarusuario.clientesusuarioespecifico', {
                url: '/clientesusuarioespecifico/:id',
                templateUrl: 'assets/pages/form-clientes-usuarioespecifico.html',
                controller: 'buscarusuarioController'
            })


            .state('buscarusuario.rolesusuarioespecifico', {
                url: '/rolesusuarioespecifico/:id',
                templateUrl: 'assets/pages/form-roles-usuarioespecifico.html',
                controller: 'buscarusuarioController'
            })
            
            .state('buscarusuario.accesosusuarioespecifico', {
                url: '/accesosusuarioespecifico/:id',
                templateUrl: 'assets/pages/form-accesos-usuarioespecifico.html',
                controller: 'buscarusuarioController'
            })
 //*****************************************************************************************************************************
            .state('crearusuario', {
                url: '/crearusuario',
                templateUrl: 'assets/pages/crearusuario.html',
                controller: 'formusuarioController'
            })

            .state('crearusuario.datosbasicosusuarios', {
                url: '/datosbasicosusuarios',
                templateUrl: 'assets/pages/form-datosbasicos-usuario.html'
            })

            .state('crearusuario.clienteusuario', {
                url: '/clienteusuario',
                templateUrl: 'assets/pages/form-cliente-usuario.html'
            })

            .state('crearusuario.rolesusuario', {
                url: '/rolesusuario',
                templateUrl: 'assets/pages/form-roles-usuario.html'
            })

            
//****************************Fin de las rutas para procesar información de Usuarios****************************************************

//****************************Rutas para procesar información de Roles******************************************************************
            .state('/roles', {
                url: '/roles',
                templateUrl: 'assets/pages/roles.html',
                controller: 'rolesController'
            })

            .state('/crearrol', {
                url: '/crearrol',
                templateUrl: 'assets/pages/crearrol.html',
                controller: 'crearrolController'
            })

            .state('/listarolesinactivos', {
                url: '/listarolesinactivos',
                templateUrl: 'assets/pages/listarolesinactivos.html',
                controller: 'listarolesinactivosController'
            })

            .state('buscarrol', {
                url: '/buscarrol/:id',
                templateUrl: 'assets/pages/datosrol.html',
                controller: 'buscarrolController'
            })
            

//****************************Fin de las rutas para procesar información de Roles********************************************************  

//****************************Rutas para procesar información de Articulos***************************************************************
            .state('/articulos', {
                url: '/articulos',
                templateUrl: 'assets/pages/articulos.html',
                controller: 'articulosController'
            })

            .state('/listaarticulosinactivos', {
                url: '/listaarticulosinactivos',
                templateUrl: 'assets/pages/listaarticulosinactivos.html',
                controller: 'listaarticulosinactivosController'
            })
 //*******************************************************************************************************************************************           
            .state('buscararticulo', {
                url: '/buscararticulo',
                templateUrl: 'assets/pages/datosarticulo.html'
            })

            .state('buscararticulo.datosbasicosarticuloespecifico', {
                url: '/datosbasicosarticuloespecifico/:id',
                templateUrl: 'assets/pages/form-datosbasicos-articulo-especifico.html',
                controller: 'buscararticuloController'
            })

            .state('buscararticulo.inventarioarticuloespecifico', {
                url: '/inventarioarticuloespecifico/:id',
                templateUrl: 'assets/pages/form-inventario-articulo-especifico.html',
                controller: 'buscararticuloController'
            })

            .state('buscararticulo.ventaarticuloespecifico', {
                url: '/ventaarticuloespecifico/:id',
                templateUrl: 'assets/pages/form-venta-articulo-especifico.html',
                controller: 'buscararticuloController'
            })

            .state('buscararticulo.recetaarticuloespecifico', {
                url: '/recetaarticuloespecifico/:id',
                templateUrl: 'assets/pages/form-receta-articulo-especifico.html',
                controller: 'buscararticuloController'
            })

            .state('buscararticulo.empaquearticuloespecifico', {
                url: '/empaquearticuloespecifico/:id',
                templateUrl: 'assets/pages/form-empaque-articulo-especifico.html',
                controller: 'buscararticuloController'
            })

//*******************************************************************************************************************************************
            .state('creararticulo', {
                url: '/creararticulo',
                templateUrl: 'assets/pages/creararticulo.html',
                controller: 'articulosController' 
            })

            .state('creararticulo.datosbasicosarticulo', {
                url: '/datosbasicosarticulo',
                templateUrl: 'assets/pages/form-datosbasicos-articulo.html',
                controller: 'Controller'
            })

            .state('creararticulo.inventarioarticulo', {
                url: '/inventarioarticulo',
                templateUrl: 'assets/pages/form-inventario-articulo.html',
                controller: 'Controller'
 
            })

            .state('creararticulo.ventaarticulo', {
                url: '/ventaarticulo',
                templateUrl: 'assets/pages/form-venta-articulo.html',
                controller: 'Controller'
            })

            .state('creararticulo.recetaarticulo', {
                url: '/recetaarticulo',
                templateUrl: 'assets/pages/form-receta-articulo.html',
                controller: 'Controller'
            })

            .state('creararticulo.empaquearticulo', {
                url: '/empaquearticulo',
                templateUrl: 'assets/pages/form-empaque-articulo.html',
                controller: 'Controller'
            })



//****************************Fin de las rutas para procesar información de Articulos**************************************************** 


//****************************Rutas para procesar información de Grupos de Selección*****************************************************
            .state('/gruposdeseleccion', {
                url: '/gruposdeseleccion',
                templateUrl: 'assets/pages/gruposdeseleccion.html',
                controller: 'gruposdeseleccionController'
            })

            .state('/listagruposeleccioninactivos', {
                url: '/listagruposeleccioninactivos',
                templateUrl: 'assets/pages/listagruposeleccioninactivos.html',
                controller: 'listagruposeleccioninactivosController'
            })

            .state('/creargruposdeseleccion', {
                url: '/creargruposdeseleccion',
                templateUrl: 'assets/pages/creargruposdeseleccion.html',
                controller: 'creargruposeleccionController'
            })
            
            .state('buscargruposeleccion', {
                url: '/buscargruposeleccion/:id',
                templateUrl: 'assets/pages/datosgruposeleccion.html',
                controller: 'buscargruposeleccionController'
            })

            

//****************************Fin de las rutas para procesar información de Grupos de Selección*******************************************


//****************************Rutas para procesar información de Lista de Precios*********************************************************
            .state('/listaprecios', {
                url: '/listaprecios',
                templateUrl: 'assets/pages/listaprecios.html',
                controller: 'listapreciosController'
            })

            .state('/crearlistaprecios', {
                url: '/crearlistaprecios:id',
                templateUrl: 'assets/pages/crearlistaprecios.html',
                controller: 'crearlistapreciosController'
            })

            .state('/listapreciosinactivos', {
                url: '/listapreciosinactivos',
                templateUrl: 'assets/pages/listapreciosinactivos.html',
                controller: 'listapreciosinactivosController'
            })
            


//****************************Fin de las rutas para procesar información de Lista de Precios**********************************************


//****************************Rutas para procesar información de Lista de Clases*********************************************************
            .state('/clases', {
                url: '/clases',
                templateUrl: 'assets/pages/clases.html',
                controller: 'clasesController'
            })

            .state('/listaclasesinactivas', {
                url: '/listaclasesinactivas',
                templateUrl: 'assets/pages/listaclasesinactivas.html',
                controller: 'listaclasesinactivasController'
            })

            .state('/crearclases', {
                url: '/crearclases',
                templateUrl: 'assets/pages/crearclases.html',
                controller: 'crearclasesController'
            })           
            
            .state('buscarclase', {
                url: '/buscarclase/:id',
                templateUrl: 'assets/pages/datosclases.html',
                controller: 'buscarclaseController'
            })

//****************************Fin de las rutas para procesar información de Lista de Clases**********************************************		


//****************************Rutas para procesar información de Puntos******************************************************************    
            .state('/puntos', {
                url: '/puntos',
                templateUrl: 'assets/pages/puntos.html',
                controller: 'puntosController'
            })
            .state('/listapuntosinactivos', {
                url: '/listapuntosinactivos',
                templateUrl: 'pages/listapuntosinactivos.html',
                controller: 'listapuntosinactivosController'
            })
//***************************crear puntos***************************************************************************************************            
            .state('crearpunto', {
                url: '/crearpunto',
                templateUrl: 'assets/pages/crearpunto.html',
                controller: 'formpuntoController'
                
                
            })
            .state('crearpunto.datosbasicospunto', {
                url: '/datosbasicospunto/:id',
                templateUrl: 'assets/pages/form-datosbasicos-punto.html',
                controller: 'crearpuntoController'
            })


            .state('crearpunto.configuracionpunto', {
                url: '/configuracionpunto/:id/:cond',
                templateUrl: 'assets/pages/form-configuracion-punto.html',
                controller: 'crearpuntoController'

            })


            .state('crearpunto.marcasbodegaspunto', {
                url: '/marcasbodegaspunto/:id/:cond',
                templateUrl: 'assets/pages/form-marcasbodegas-punto.html',
                controller: 'crearpuntoController'
            })
            
//*******************************************************************************************************************************************            
            .state('buscarpuntoscliente', {
                url: '/buscarpuntoscliente/:id',
                templateUrl: 'assets/pages/puntoscliente.html',
                controller: 'buscarpuntosclienteController'
            })
            
            .state('buscarpuntoespecifico', {
                url: '/buscarpuntoespecifico',
                templateUrl: 'assets/pages/datospuntosespecificos.html'
                
            })

            .state('buscarpuntoespecifico.datosbasicospuntoespecifico', {
                url: '/datosbasicospuntoespecifico/:id',
                templateUrl: 'assets/pages/form-datosbasicos-puntoespecifico.html',
                controller: 'buscarpuntosespecificosController'
            })


            .state('buscarpuntoespecifico.configpuntoespecifico', {
                url: '/configpuntoespecifico/:id',
                templateUrl: 'assets/pages/form-conf-puntoespecifico.html',
                controller: 'buscarpuntosespecificosController'
            })


            .state('buscarpuntoespecifico.marcabodegapuntoespecifico', {
                url: '/marcabodegapuntoespecifico/:id',
                templateUrl: 'assets/pages/form-marcabodega-puntoespecifico.html',
                controller: 'buscarpuntosespecificosController'
               
            })
            
            .state('buscarpuntosarticulo', {
                url: '/buscarpuntosarticulo/:id',
                templateUrl: 'assets/pages/puntosarticulo.html',
                controller: 'buscarpuntosarticuloController'
            })
            
            //****************************Rutas para procesar información de Compras**********************************************************           
            .state('/compras', {
                url: '/compras',
                templateUrl: 'assets/pages/compras.html',
                controller: 'comprasController'
            })
            
            .state('buscarcompra', {
                url: '/buscarcompra',
                templateUrl: 'assets/pages/buscarcompra.html',
                controller: 'buscarcompraController'
            });

           
//*****************************************************************************************************************

    $urlRouterProvider.otherwise('/');
});







